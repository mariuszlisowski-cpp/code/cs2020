cmake_minimum_required(VERSION 2.6)

project(game)
set(GTEST_DIR  /usr/src/googletest/googletest)
ADD_SUBDIRECTORY(${GTEST_DIR} gtest)

enable_testing()
add_executable(${PROJECT_NAME}-ut game-ut.cpp)
target_link_libraries(${PROJECT_NAME}-ut gtest_main)
add_test(NAME GameTest COMMAND ${PROJECT_NAME}-ut)
