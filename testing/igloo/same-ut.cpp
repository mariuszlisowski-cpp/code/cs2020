// test compares if vectors are of the same size
// ~ compile simply with 'g++ <test>.cpp -o <test>
// ~ run '<test> --help' for options
// ~ run '<test> --output=color' with colorfull output

#include "same.cpp"

void doTest(std::vector<int> a, std::vector<int> b, bool sol) {
    bool ans = Same::comp(a, b);
    igloo::Assert::That(ans, igloo::Equals(sol));
}

Describe(SizeOfElementsComparisionTests) {
    std::vector<int> a;
    std::vector<int> b;

    // edge cases
    It(compareEmptyBothVectors) {
        a = {};
        b = {};
        doTest(a, b, true);
    }
    It(compareEmptyFirstVector) {
        a = { 1 };
        b = {};
        doTest(a, b, false);
    }
    It(compareEmptySecondVector) {
        a = {};
        b = { 1 };
        doTest(a, b, false);
    }
    It(compareVectorsOfDifferentSizes) {
        a = { 1, 2 };
        b = { 1, };
        doTest(a, b, false);
    }
    It(compareVectorsOfSameSizes) {
        a = { 1, 2 };
        b = { 2, 1 };
        doTest(a, b, true);
    }
};
