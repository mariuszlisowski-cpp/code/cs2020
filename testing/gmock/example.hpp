#pragma once

#include <string>

class Example {
public:
    // virtual ~Example();

    virtual int GetSize() const = 0;
    virtual std::string Describe(const char* name) = 0;
    virtual std::string Describe(int type) = 0;
    virtual bool Process(int count) = 0;
};
