#include <gmock/gmock.h>

#include "example.hpp"
#include "example_mock.hpp"

using ::testing::Return;

TEST(ExampleTest, ShouldDoSth) {
    MockExample mockExample;

    EXPECT_CALL(mockExample, Describe(5))
      .Times(3)
      .WillRepeatedly(Return("Category 5"));
}
