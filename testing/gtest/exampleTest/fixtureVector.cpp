#include <gtest/gtest.h>
#include <vector>

////////////////////////////////////////////////////
class Stack {
public:
    void push(size_t value) {
        vstack.push_back(value);
    }
    int pop() {
        if (vstack.size()) {
            size_t value = vstack.back();
            vstack.pop_back();
            return value;
        } else {
            return -1;
        }
    }
    size_t getSize() {
        return vstack.size();
    }

private:
    std::vector<size_t> vstack{};
};
// fixture
struct StackTest : public testing::Test {
    Stack stack;
    StackTest() {
        size_t values[]{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        for (const auto& el : values) {
            stack.push(el);
        }
    }
    ~StackTest() {}
};
// fixture tests
TEST_F(StackTest, PopTest) {
    int lastPoppedValue = 9;
    while (lastPoppedValue != 1) {
        ASSERT_EQ(stack.pop(), lastPoppedValue--);
    }
}
TEST_F(StackTest, SizeValidityTest) {
    int val = stack.getSize();
    for (val; val > 0; --val) {
        ASSERT_NE(stack.pop(), -1);
    }
}
////////////////////////////////////////////////////
int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
