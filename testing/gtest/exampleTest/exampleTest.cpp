#include <iostream>
#include <gtest/gtest.h>
#include <string>

////////////////////////////////////////////////////
TEST(Assert, FatalFailure) {
    ASSERT_TRUE(1 == 2); // fatal failure
    std::cout << "will not execute" << std::endl;
}
TEST(Expect, NonFatalFailure) {
    EXPECT_FALSE(1 == 1); // non-fatal failure
    std::cout << "will execute" << std::endl;
}
////////////////////////////////////////////////////
TEST(IncrementTest, IncrementBy2) {
    // arrange (given)
    int value = 10;
    int increment = 2;
    // act  (when)
    value = value + increment;
    // assert (then)
    ASSERT_EQ(12, value);
}
////////////////////////////////////////////////////
class Password {
public:
    Password() = delete;
    Password(std::string pass):pass_(pass) {}

    std::string getPass() {
        return pass_;
    }

private:
    std::string pass_;
};

TEST(PasswordTest, ShouldReturnPassToor) {
    // given
    Password password("toor");
    // when
    std::string pass = password.getPass();
    // then
    ASSERT_EQ(pass, "toor");
}

TEST(PasswordTest, ShourReturnPassEmpty) {
    // arrange
    Password password("");
    // act
    std::string pass = password.getPass();
    // assert
    ASSERT_STREQ(pass.c_str(), ""); // c string pointer
}
////////////////////////////////////////////////////
int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
