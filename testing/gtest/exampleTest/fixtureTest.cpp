#include <iostream>
#include <gtest/gtest.h>

////////////////////////////////////////////////////
class Increment {
public:
    Increment(int value):value_(value) {}
    
    void incrementValue(int byValue) {
        value_ += byValue;
    }
    int getValue() {
        return value_;
    }

private:
    int value_;
};

struct FixtureIncrement : public testing::Test {
    Increment* incrementPtr{};
    
    // SetUp routine
    void SetUp() {
        incrementPtr = new Increment(10);
        std::cout << "Allocation of resources" << '\n';
    }
    // or in constructor
    // FixtureIncrement() { incrementPtr = new Increment(10); }
    
    // TearDown routine (can handle exceptions)
    void TearDown() {
        delete incrementPtr;
        std::cout << "Deallocation of resources" << '\n';
        std::cout << "Dead" << '\n';}
    // or in destructor
    // ~FixtureIncrement() { delete incrementPtr; }
};

TEST_F(FixtureIncrement, IncrementBy5) {
    // given
    incrementPtr->incrementValue(5);
    // then
    ASSERT_EQ(incrementPtr->getValue(), 15);
}
TEST_F(FixtureIncrement, IncrementBy2) {
    // given
    incrementPtr->incrementValue(2);
    // then
    ASSERT_EQ(incrementPtr->getValue(), 12);
}
////////////////////////////////////////////////////
int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
