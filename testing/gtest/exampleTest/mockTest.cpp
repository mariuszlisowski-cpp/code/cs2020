#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>

using ::testing::_;
using ::testing::AtLeast;
using ::testing::Return;

class DataBaseConnect {
public:
    virtual bool login(std::string username, std::string password) {
        return true;
    }
    virtual bool logout(std::string username) {
        return true;
    }
    virtual int fetchRecord() {
        return -1;
    }
};

class MockDatabase : public DataBaseConnect {
public:
    MOCK_METHOD0(fetchRecord, int());
    MOCK_METHOD1(logout, bool(std::string username));
    MOCK_METHOD2(login, bool(std::string username, std::string password));
};

class MyDatabase {
public:
    MyDatabase(DataBaseConnect& dataBaseConnect)
        : dataBaseConnect_(dataBaseConnect)
    {}

    int init(std::string username, std::string password) {
        if(dataBaseConnect_.login(username, password)) {
            std::cout << "Database connect success!" << '\n';
            return 1;
        } else {
            std::cout << "Database connect failure!" << '\n';
            return -1;
        }
    }

private:
    DataBaseConnect& dataBaseConnect_;
};

TEST(MyDatabaseTest, LoginTest) {
    // arrange
    MockDatabase mockDatabase;
    MyDatabase database(mockDatabase);
    EXPECT_CALL(mockDatabase, login("root", "toor")) // login(_, _)
    .Times(AtLeast(1))
    .WillOnce(Return(true));
    // act
    int returnValue = database.init("root", "toor");
    // assert
    EXPECT_EQ(returnValue, 1);
}
