#include <gtest/gtest.h>
#include <string>

#include "anagram.cpp"

class AnagramConfirmTest : public ::testing::TestWithParam<std::pair<std::string, std::string>> {
};
class AnagramRejectTest : public ::testing::TestWithParam<std::pair<std::string, std::string>> {
};

TEST_P(AnagramConfirmTest, ShouldConfirmThatAnagrams){
    EXPECT_TRUE(isAnagram(GetParam().first, GetParam().second));
}

TEST_P(AnagramRejectTest, ShouldRejectThatAnagrams){
    EXPECT_FALSE(isAnagram(GetParam().first, GetParam().second));
}

// TRUE
INSTANTIATE_TEST_CASE_P(WordsSameCase,
                         AnagramConfirmTest,
                         ::testing::Values(
                             std::make_pair("cat", "act"),
                             std::make_pair("listen", "silent"),
                             std::make_pair("STATE", "TASTE")
                         ));

INSTANTIATE_TEST_CASE_P(WordsMixedCase,
                         AnagramConfirmTest,
                         ::testing::Values(
                             std::make_pair("cat", "ACT"),
                             std::make_pair("LISTEN", "silent"),
                             std::make_pair("STATE", "taste")
                         ));

INSTANTIATE_TEST_CASE_P(SentencesMixedCase,
                         AnagramConfirmTest,
                         ::testing::Values(
                             std::make_pair("School master", "The classroom"),
                             std::make_pair("The eyes", "They see"),
                             std::make_pair("Eleven plus two", "Twelve plus one")
                         ));

INSTANTIATE_TEST_CASE_P(EmptyStrings,
                         AnagramConfirmTest,
                         ::testing::Values(
                             std::make_pair("", "")
                         ));

// FALSE
INSTANTIATE_TEST_CASE_P(SentencesNotAnagrams,
                        AnagramRejectTest,
                        ::testing::Values(
                            std::make_pair("SSchool master", "The classroom"),
                            std::make_pair("The eyes", "They seeE"),
                            std::make_pair("Eleven plus two", "tTwelve plus one")
                        ));

INSTANTIATE_TEST_CASE_P(EmptyStringsNotAnagrams,
                        AnagramRejectTest,
                        ::testing::Values(
                            std::make_pair("", "The classroom"),
                            std::make_pair("The eyes", "")
                        ));
