cmake_minimum_required(VERSION 2.6)

project(anagram)

set(GTEST_DIR /Users/Raisin/Develop/repository/gitHub/googletest/)
ADD_SUBDIRECTORY(${GTEST_DIR} gtest)

# if (CMAKE_VERSION VERSION_LESS 2.8.11)
#     include_directories(${gtest_SOURCE_DIR}/inc)
# endif()


add_executable(${PROJECT_NAME}-ut anagram-ut.cpp)
target_link_libraries(${PROJECT_NAME}-ut gtest_main)

enable_testing()
add_test(NAME anagram_compare COMMAND anagram-ut)
