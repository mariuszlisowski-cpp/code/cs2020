// anagram comparison
// ~ words/sentences having a meaning

#include <algorithm>
#include <iostream>

using namespace std;

bool isAnagram(string in1, string in2) {
    // different sizes
    if (in1.size() != in2.size())
        return false;

    // case insensitive
    transform(in1.begin(), in1.end(), in1.begin(), ::tolower);
    transform(in2.begin(), in2.end(), in2.begin(), ::tolower);

    // removing spaces
    in1.erase(remove(in1.begin(), in1.end(), ' '), in1.end());
    in2.erase(remove(in2.begin(), in2.end(), ' '), in2.end());

    // is output the same
    return is_permutation(in1.begin(), in1.end(),
                          in2.begin(), in2.end());
}
