#include "gtest/gtest.h"

#include <algorithm>
#include <string>
#include <vector>

// test of gtest: should pass
TEST(FooTest, DoseXYZ) {
    ASSERT_TRUE(true);
}
////////////////////////////////////////////////////////////////////////////
TEST(SortTests, GivenAStringTextWhenTExIsSortedThenItIsEqualsToEttx) {
    // GIVEN
    std::string text{ "text" };
    std::string expected{ "ettx" };
    // WHEN
    std::sort(std::begin(text), std::end(text));
    // THEN
    ASSERT_EQ(text, expected);
}
////////////////////////////////////////////////////////////////////////////
TEST(SortTests, SortingEmptyVectorStillGivesAnEmpytyVector) {
    // GIVEN
    std::vector<int> given{};
    std::vector<int> expected{};
    // WHEN
    std::sort(std::begin(given), std::end(given));
    // THEN
    EXPECT_EQ(given, expected);
}
////////////////////////////////////////////////////////////////////////////
// Fixture
class SortTestFixture : public ::testing::Test {
public:
    std::vector<int> vectorGiven{ 3, 2, 1, 4 };
    std::vector<int> vectorExpectedAsc{ 1, 2, 3, 4 };
    std::vector<int> vectorExpectedDes{ 4, 3, 2, 1 };

    std::string stringGiven{ "text" };
    std::string stringExpectedAsc{ "ettx" };
    std::string stringExpectedDes{ "xtte" };

    template<typename T>
    void sortAscending(T& given,
                      const T& expected)
    {
        // WHEN
        std::sort(std::begin(given), std::end(given));
        // THEN
        ASSERT_EQ(given, expected);
    }
    template<typename T>
    void sortDescending(T& given,
                        const T& expected)
    {
        // WHEN
        std::sort(std::begin(given),
                  std::end(given),
                  std::greater<int>()); // error when T
        // THEN
        ASSERT_EQ(given, expected);
    }
};

TEST_F(SortTestFixture, GivenAContainerWhenSortedThenEquals_) {
    sortAscending(vectorGiven, vectorExpectedAsc);
    sortDescending(vectorGiven, vectorExpectedDes);

    sortAscending(stringGiven, stringExpectedAsc);
    sortDescending(stringGiven, stringExpectedDes);
}
////////////////////////////////////////////////////////////////////////////
// Fixture (with parameter)
class Fixture : public ::testing::TestWithParam<int> {
public:
};

TEST_P(Fixture, Name) {
    auto value = GetParam();
    auto result = value;
    EXPECT_EQ(value, result);
}

INSTANTIATE_TEST_SUITE_P(InstantiationName,
                         Fixture,
                         testing::Values(1, 2, 3));
////////////////////////////////////////////////////////////////////////////
// Fixture (with parameter)
class MyFixture : public ::testing::TestWithParam<std::pair<int, int>> {
public:
};

TEST_P(MyFixture, MyName) {
    auto pair = GetParam();
    auto valuePositive = pair.first;
    auto valueNegative = pair.second;

    EXPECT_EQ(valuePositive, -valueNegative);
}

INSTANTIATE_TEST_SUITE_P(InstantiationName,
                         MyFixture,
                         testing::Values(std::make_pair(1, -1),
                                         std::make_pair(2, -2)));
////////////////////////////////////////////////////////////////////////////
// Fixture (with parameter)
using VectorPair = std::pair<std::vector<int>, std::vector<int>>;

class TestFixture : public testing::TestWithParam<VectorPair> {
public:
};

TEST_P(TestFixture, TestName) {
    // GIVEN
    auto [vectorGiven, vectorExpectedAsc] = GetParam(); // c++17
    // WHEN
    std::sort(std::begin(vectorGiven), std::end(vectorGiven));
    // THEN
    EXPECT_EQ(vectorGiven, vectorExpectedAsc);
}

INSTANTIATE_TEST_CASE_P(PermutationsOfPairOfVectors,
                         TestFixture,
                         testing::Values(VectorPair{{3, 2, 1}, {1, 2, 3}},
                                         VectorPair{{3, 1, 2}, {1, 2, 3}},
                                         VectorPair{{2, 1, 3}, {1, 2, 3}},
                                         VectorPair{{2, 3, 1}, {1, 2, 3}},
                                         VectorPair{{1, 2, 3}, {1, 2, 3}},
                                         VectorPair{{1, 3, 2}, {1, 2, 3}}));
////////////////////////////////////////////////////////////////////////////
