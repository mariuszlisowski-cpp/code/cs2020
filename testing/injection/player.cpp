#include "player.hpp"

Player::Player(const std::shared_ptr<Weapon>& defaultWeapon) :
    currentWeapon_(defaultWeapon)
{
    weapons_.emplace_back(defaultWeapon);
}

void Player::shoot() {
    currentWeapon_->shoot();
}
void Player::reload() {
    currentWeapon_->reload();
}
