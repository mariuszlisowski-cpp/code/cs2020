#include <gtest/gtest.h>

#include "square.cpp"

TEST(SquareRootTest, PositiveNos) {
    ASSERT_EQ(6, squareRoot(36.0));
    ASSERT_EQ(25.4, squareRoot(645.16));
}

TEST(SquareRootTest, NegativeNos) {
    ASSERT_EQ(-1.0, squareRoot(-10));
    ASSERT_EQ(-1.0, squareRoot(-0.5));
}
// disabled test
TEST(DISABLED_SquareRootTest, ZeroNo) {
    ASSERT_EQ(0.0, squareRoot(0.0));
}
// disabled class
class DISABLED_TestClass : public ::testing::Test {};

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
