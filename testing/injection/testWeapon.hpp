#pragma once

#include "weapon.hpp"

class TestWeapon : public Weapon {
public:
    bool hasShot = false;
    bool hasReload = false;

    void shoot() override;
    void reload() override;
};
