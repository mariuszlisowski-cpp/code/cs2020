// testing with injected TestWeapon class

#include <gtest/gtest.h>

#include "gun.hpp"
#include "player.hpp"
#include "testWeapon.hpp"
#include "weapon.hpp"

TEST(ShootTest, ShouldShootAndReloadWeapon) {
    auto testWeapon = std::make_shared<TestWeapon>();
    Player player(testWeapon);

    player.shoot();
    EXPECT_TRUE(testWeapon->hasShot);
    EXPECT_FALSE(testWeapon->hasReload);
    
    player.reload();
    EXPECT_TRUE(testWeapon->hasReload);
    EXPECT_FALSE(testWeapon->hasShot);
}
