#pragma once

#include <memory>
#include <vector>

#include "weapon.hpp"

class Player {
public:
    Player(const std::shared_ptr<Weapon>& defaultWeapon);

    void shoot();
    void reload();

private:
    std::vector<std::shared_ptr<Weapon>> weapons_;
    std::shared_ptr<Weapon> currentWeapon_;
};
