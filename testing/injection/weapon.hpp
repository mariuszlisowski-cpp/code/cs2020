#pragma once

class Weapon {
public:
    virtual ~Weapon() = default;

    virtual void shoot() = 0;
    virtual void reload() = 0;
};
