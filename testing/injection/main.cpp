// 

#include <iostream>

#include "weapon.hpp"
#include "gun.hpp"
#include "player.hpp"

int main() {
    std::shared_ptr<Gun> ak47 = std::make_shared<Gun>();
    Player player(ak47);

    player.shoot();
    player.reload();

    return 0;
}