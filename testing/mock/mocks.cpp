#include <gmock/gmock.h>
#include <iostream>

class Car;
class DummyCar;
class StubCar;

// original
double Car::accelerate(int) { /* complicated implementation */ }
void sayHello(std::string name) { std::cout << "Hello " << name << '\n'; }

// dummy
double DummyCar::accelerate(int) { return 0.0; }
void dummyHello(std::string name) {}

// stub (fake)
double StubCar::accelerate(int value) {
    return value < 0 ? -10.0 : 10.0;
}
void stubHello(std::string name) {
    if (name == "anonymous") {
        throw std::logic_error("Anonymous not allowed");
    }
}
// mock
class MockCar : public Car {
    MOCK_METHOD(double, accelerate, (int), (override));
};

// spy (actions registering)
