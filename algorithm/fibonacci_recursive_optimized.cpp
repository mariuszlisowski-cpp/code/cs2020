// fibonacci sequence
// optimized by 'constexpr'

#include <iostream>

using namespace std;

// returns number n of fibonacci sequence
constexpr long fib(int n) {
    if (n == 1 || n == 0)
        return n;
    return fib(n - 1) + fib(n - 2);
}

int main() {
    long result = fib(45);
    cout << result << endl;

    return 0;
}
