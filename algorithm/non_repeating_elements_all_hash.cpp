// all non repeating elements (hash table)
// ~ finds all non-repeating element in an array
// #unordered_map

#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

vector<int> findNonRepeating(int ar[], size_t size) {
    unordered_map<int, int> mp;
    vector<int> vec;
    
    // counting occurences
    for (int i = 0; i < size; ++i)
        mp[ar[i]]++;
    // traverse map	
    for (auto m : mp) {
        cout << m.first << " : " << m.second << endl;
        // push all keys with value one
        if (m.second == 1)
            vec.push_back(m.first);
    }
    return vec;
}

int main() {
    int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5};
    size_t n = sizeof(arr) / sizeof(arr[0]);
    
    vector<int> v = findNonRepeating(arr, n);
    for (auto e : v)
        cout << e << " ";

    return 0;
}