// lcm & gcd
// LCM [NWW] (najmniejsza wspolna wielokrotnosc)
// GCD [NWD] (najwiekszy wspolny dzielnik)

#include <iostream>
#include <numeric>

// least common multiple
unsigned long long LCM(long long lhs, long long rhs) {
    return std::lcm(lhs, rhs);
}

// greatest common divisor
unsigned long long GCD(long long lhs, long long rhs) {
    return std::gcd(lhs, rhs);
}

int main() {
    int a = 100000000 - 1;
    int b = 100000000 + 1;

    std::cout << "LCM(" << a << ", " << b << ") = " << LCM(a, b) << "\n";
    std::cout << "GCD(" << a << ", " << b << ") = " << GCD(a, b) << "\n";

  return 0;
}
