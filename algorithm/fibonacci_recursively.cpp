// fibonacci recursively
// ~ under linux run with preceding 'time ./fibonacci'
// ~ to measure execution time with and withour constexpr
// 0
// 1
// 1 + 0 = 1
// 1 + 1 = 2
// 2 + 1 = 3
// 3 + 2 = 5
// 5 + 3 = 8
// 8 + 5 = 13
// 13 + 8 = 21
// 0 1 1 2 3 5 8 13 21...

#include <iostream>

// n = 0 -> 0
// n = 1 -> 1
// n > 1 -> fib(n -1) + fib(n - 2)
long fib(long n) {
    if (n == 0) { return 0; }
    if (n == 1) { return 1; }

    return fib(n - 1) + fib(n - 2);
}

// regular (short)
long fibR(long n) {
    return n <= 2 ? 1 : fibR(n - 1) + fibR(n - 2);
}
// constexpr (short)
constexpr long fibC(int n) {
    return n <= 2 ? 1 : fibC(n - 1) + fibC(n - 2);
}

int main() {
    std::cout << "Calculating regular Fibonacci..." << std::endl;
    long fibonacciR = fibR(44);  // about 4 secss
    std::cout << fibonacciR << std::endl;

    std::cout << "Ready constexpr Fibonacci..." << std::endl;
    constexpr long fibonacciC = fibC(44); // immediately
    std::cout << fibonacciC << std::endl;

    return 0;
}
