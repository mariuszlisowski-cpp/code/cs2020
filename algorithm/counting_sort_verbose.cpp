// counting sort (verbose)
// limitations: UNSIGNED only (0-n)

#include <iostream>
#include <vector>
#include <algorithm>

// counting sort
std::vector<int> createSortedList(const std::vector<int> &v) {
    std::vector<int> result;

    size_t countSize = *std::max_element(std::begin(v), std::end(v)) + 1;
    std::cout << "Array size: " << countSize << std::endl;

    int * countArr = new int[countSize] {};
    // counting occurences of elements in vector
    for (int i = 0; i < v.size(); ++i) {
        std::cout << "Vec el " << v[i] << " : ";
        ++countArr[v[i]];
        std::cout << countArr[v[i]] << " occurence(s)" << std::endl;
    }

    for (int i = 0; i < countSize; ++i) {
        while (countArr[i] > 0) {
            result.push_back(i);
            --countArr[i];
        }
    }
    return result;
}

int main() {
    std::vector<int> vec{2, 3, 2, 1, 0, 5, 8, 0, 9, 0, 22, 19};

    auto list = createSortedList(vec);

    for (const auto& el : list)
        std::cout << el << " ";

    return 0;
}