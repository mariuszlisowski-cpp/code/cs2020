// setters & getters example

#include <iostream>
#include <string>

class Ship {
public:
    // setters
    void setId(const size_t& id) { id_ = id; }
    void setMaxCrew(const size_t maxCrew) { maxCrew_ = maxCrew; }
    void setCapacity(const size_t capacity) { capacity_ = capacity; }
    void setSpeed(const double speed) { speed_ = speed; }
    void setName(const std::string& name) { name_ = name; }
    // getters
    size_t getId() const { return id_; }
    size_t getMaxCrew() const { return maxCrew_; }
    size_t getCapacity() const { return capacity_; }
    double getSpeed() const { return speed_; }
    std::string getName() const { return name_; }

private:
    size_t id_, maxCrew_, capacity_;
    double speed_;
    std::string name_;
};

int
main() {
    Ship titanic;

    return 0;
}