// setter & getter

#include <iostream>
#include <string>

class Person {
public:
    // default c-tor
    Person() {} // may be absent
    // setter
    void setName(const std::string& name) {
        name_ = name;
    }
    // getter (always CONST)
    std::string getName() const {
        return name_;
    }

private:
    std::string name_;
};

int main() {
    Person person;
    person.setName("Mateusz");

    std::cout << person.getName() << '\n';

    return 0;
}