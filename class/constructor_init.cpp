// constructor init

#include <iostream>

class Point2D {
public:
    Point2D(size_t x, size_t y)
        : x_(x), y_(y) {}
    // default c-tor
    Point2D() {} // initialized in private 
    // or..
    //Point2D(): Point2D(0, 0) {} // via above c-tor
    // or..
    //Point2D(): x_(0), y_(0) {} // initialize list

    void set_x(size_t x) {
        x_ = x;
    };
private:
    size_t x_ = 0;
    size_t y_ = 0;
};

main() {
    Point2D point(0, 0);

    return 0;
}