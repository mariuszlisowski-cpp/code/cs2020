// operator overloading ship

#include <algorithm>
#include <iostream>
#include <string>

class Ship {
public:
    // few arguments
    Ship(size_t maxCrew,
         std::string name)
        : maxCrew_(maxCrew), name_(name) {}
    // all arguments
    Ship(size_t id,
         size_t maxCrew,
         size_t capacity,
         double speed,
         std::string name)
        : id_(id), maxCrew_(maxCrew), capacity_(capacity), speed_(speed), name_(name) {}
    // default (delegate to another c-tor)
    Ship()
        : Ship(0, 0, 0, 0, "boat") {}

    // setters
    void setId(const size_t& id) { id_ = id; }
    void setMaxCrew(const size_t maxCrew) { maxCrew_ = maxCrew; }
    void setCapacity(const size_t capacity) { capacity_ = capacity; }
    void setSpeed(const double speed) { speed_ = speed; }
    void setName(const std::string& name) { name_ = name; }
    // getters
    size_t getId() const { return id_; }
    size_t getMaxCrew() const { return maxCrew_; }
    size_t getCapacity() const { return capacity_; }
    size_t getCrew() const { return crew_; }
    double getSpeed() const { return speed_; }
    std::string getName() const { return name_; }

    // operators (always returns class ref)
    Ship& operator+=(const size_t crew) {
        if (crew + crew_ > maxCrew_) {
            std::cerr << "Maximum crew reached!\n";
            return *this;
        } else {
            crew_ += crew;
            return *this;
        }
    }
    Ship& operator-=(const size_t crew) {
        if (crew > crew_) {
            std::cerr << "Number of papays reached 0\n";
            return *this;
        } else {
            crew_ -= crew;
            return *this;
        }
    }
private:
    size_t id_, maxCrew_, capacity_, crew_ = 0;
    double speed_;
    std::string name_;
};

int main() {
    Ship ship(99, "titanic");

    ship += 10;  // operator+= used
    ship += 5;
    // ship -= 5;  // operator-= used

    std::cout << ship.getName() << " : "
              << ship.getCrew() << " papays\n";

    return 0;
}