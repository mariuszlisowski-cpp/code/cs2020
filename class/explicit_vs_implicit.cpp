// explicit vs implicit

#include <iostream>

class Foo {
public:
    void printNum() {
        std::cout << ++num_ << '\n';
    }
    // do not modify class variables
    void doNotModify() const {
        // num_++; // ERROR
    }
    // explicit vs implicit
    explicit Foo(int num): // explicit conversion only
        num_(num) {}
private:
    int num_;
};

void bar(Foo obj) {
    obj.printNum();
}

int main() {
    Foo object(5);
    bar(object); // constructor used explicitly
    
    // ERROR if c-tor is explicit
    //bar(10); // here: implicit conversion

    return 0;
}