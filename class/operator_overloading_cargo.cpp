// operator overloading cargo

#include <iostream>
#include <string>

class Cargo {
public:
    // getter
    size_t getAmount() const {
        return amount_;
    }

    // operators (class ref returned)
    Cargo& operator+=(const size_t amount) {
        amount_ += amount;
        return *this;
    }
    Cargo& operator-=(const size_t amount) {
        amount_ -= amount;
        return *this;
    }
private:
    size_t amount_;
    size_t base_price;
    std::string name_;
};

int main() {
    Cargo bananas;

    bananas += 99;
    bananas -= 9;

    std::cout << bananas.getAmount() << '\n';

    return 0;
}