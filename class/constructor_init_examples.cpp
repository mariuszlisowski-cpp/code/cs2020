// constructor init examples

#include <iostream>
#include <string>

class Ship {
public:
    // few arguments
    Ship(size_t id,
         std::string name)
        : id_(id), name_(name) {}
    // all arguments
    Ship(size_t id,
         size_t maxCrew,
         size_t capacity,
         double speed,
         std::string name)
        : id_(id), maxCrew_(maxCrew), capacity_(capacity), speed_(speed), name_(name) {}
    // default (delegate to another c-tor)
    Ship()
        : Ship(0, 0, 0, 0, "boat") {}

    // setters
    void setId(const size_t& id) { id_ = id; }
    void setMaxCrew(const size_t maxCrew) { maxCrew_ = maxCrew; }
    void setCapacity(const size_t capacity) { capacity_ = capacity; }
    void setSpeed(const double speed) { speed_ = speed; }
    void setName(const std::string& name) { name_ = name; }
    // getters
    size_t getId() const { return id_; }
    size_t getMaxCrew() const { return maxCrew_; }
    size_t getCapacity() const { return capacity_; }
    double getSpeed() const { return speed_; }
    std::string getName() const { return name_; }

private:
    size_t id_, maxCrew_, capacity_;
    double speed_;
    std::string name_;
};

int main() {
    Ship boat;

    std::cout << boat.getId() << " : "
              << boat.getName() << '\n';

    Ship ship(999, "titanic");

    std::cout << ship.getId() << " : "
              << ship.getName() << '\n';

    return 0;
}