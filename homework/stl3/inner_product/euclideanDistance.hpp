#pragma once

#include <vector>

double euclideanDistance(const std::vector<int>& vecA, const std::vector<int>& vecB);
