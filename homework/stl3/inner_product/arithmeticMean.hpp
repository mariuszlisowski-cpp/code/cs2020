#pragma once

#include <vector>

double arithmeticMean(const std::vector<int>& vecA, const std::vector<int>& vecB);
