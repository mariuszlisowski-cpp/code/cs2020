// virtual vs regualr method

#include <iostream>

struct Bird {
    // regular
    void sing1() { std::cout << "tweet, tweet\n"; }
    // virtual
    virtual void sing2() { std::cout << "tweet, tweet\n"; }
};

struct Sparrow : Bird {
    void sing2() override { std::cout << "chirp, chirp\n"; }
};

int main() {
    Sparrow sparrow;
 
    Bird& bird1 = sparrow;
    bird1.sing1(); // Bird method (nothing to override)

    Bird& bird2 = sparrow;
    bird2.sing2(); // Sparrow method (virtual overridden)
 
    return 0;
}

