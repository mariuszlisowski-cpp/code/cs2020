// non copyable class 

class NonCopyable {
public:
    NonCopyable() = default;
    // copying
    NonCopyable(const NonCopyable&) = delete;
    NonCopyable& operator=(const NonCopyable&) = delete;
    // moving
    NonCopyable(NonCopyable&&) = default;
    NonCopyable& operator=(NonCopyable&&) = default;
};

int main() {
    NonCopyable object;

    NonCopyable new_object = std::move(object);
    // NonCopyable new_object = object; // no copying

    return 0;
}
