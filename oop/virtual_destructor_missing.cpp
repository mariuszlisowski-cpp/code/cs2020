// virtual destructor missing
// ~ program without virtual destructor
//   causing undefined behavior

#include <iostream>

using namespace std;

class Base {
public:
    Base() { cout << "Constructing Base \n"; }
    ~Base() { cout << "Destructing Base \n"; } // should be virtual
};

class Derived : public Base {
public:
    Derived() { cout << "Constructing Derived \n"; }
    ~Derived() { cout << "Destructing Derived \n"; } // not called 
};

int main(void) {
    Derived* derived = new Derived();
    Base* base = derived;
    delete base;

    return 0;
}
