// virtual d-ctor missing

#include <iostream>
#include <memory>
#include <string>

class Parent {
public:
    Parent() { std::cout << "PARENT C'tor called\n"; }
    ~Parent() { std::cout << "PARENT D'tor caller\n"; }
};

class Child : public Parent {
public:
    Child() { std::cout << "CHILD C'tor called\n"; }
    ~Child() { std::cout << "CHILD D'tor caller\n"; }
};

int main() {
    // Child child1;    // ok, object on stack, not a pointer

    // no parent d-ctor called (but cleaned-up by smart pointer)
    std::unique_ptr<Parent> child2 = std::make_unique<Child>();
    std::cout << std::endl;

}
