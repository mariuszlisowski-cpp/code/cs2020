// deleted function

#include <iostream>

void integral_only(int a) {}

// do not use for double
void integral_only(double d) = delete;


int main() {
    integral_only(10);  // OK because int
    
    short s = 3;
    integral_only(s);    // OK - implicit conversion to int
    
    // integral_only(3.0);  // error - use of deleted function

    return 0;
}
