// final keyword usage 

#include <iostream>

struct A {
    virtual void foo() const final {}
    // void bar() const final {}   // compilation error, only virtual
                                   // functions can be marked as final
};

struct B : A {
    // void foo() const {}         // compilation error, cannot override
                                   // function marked as final
};
int main() {
       
    return 0;
}
