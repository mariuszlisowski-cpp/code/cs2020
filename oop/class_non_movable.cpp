// non movable class

class NonMovable {
public:
    NonMovable() = default;
    // copying
    NonMovable(NonMovable&) = default;
    NonMovable& operator=(const NonMovable&) = default;
    // moving
    NonMovable(NonMovable&&) = delete;
    NonMovable& operator=(NonMovable&&) = delete;
};

int main() {
    NonMovable object;

    NonMovable new_object = object;
    // NonMovable new_object = std::move(object); // no moving

    return 0;
}
