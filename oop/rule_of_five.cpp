// rule of five
// ~ 1. destructor
//   2. copy constructor
//   3. move constructor
//   4. copy assignment operator
//   5. move assignment operator

#include <iostream>

template <typename T>
class MyClass {
public:
    MyClass() : member_{} {
        std::cout << "* default c'tor called *" << std::endl;
    }
    MyClass(T value) : member_(value) {
        std::cout << "* parametrized c'tor called *" << std::endl;
    } 
    // copy constructor
    MyClass(const MyClass& other) : member_(other.member_) {
        std::cout << "* copy c'tor called *" << std::endl;
    }
    // move constructor
    MyClass(MyClass&& other) : member_(other.member_) {
        other.member_ = 0;
        std::cout << "* move c'tor called *" << std::endl;
    }
    ~MyClass() {}

    // copy assignment operator
    MyClass& operator=(const MyClass& rhs) {
        std::cout << "* copy assignment called *" << std::endl;
        member_ = rhs.member_;
        return *this;
    }
    // move assignment operator
    MyClass& operator=(MyClass&& rhs) {
        std::cout << "* move assignment called *" << std::endl;
        if (this != &rhs) {
            member_ = rhs.member_;
            rhs.member_ = 0;
        }
        return *this;
    }

    // getter
    T getMember() const {
        return member_;
    }

private:
    T member_;
};

int main() {
    // default consttructor call
    MyClass<int> number;
    std::cout << "Number: " << number.getMember() << std::endl;
    // parametrized constructor call
    MyClass<int> object{9};
    std::cout << "Object: " << object.getMember() << std::endl;
    // copy constructor call
    MyClass<int> object_copy(object);
    std::cout << "Object copy: " << object_copy.getMember() << std::endl;
    // move constructor call
    MyClass<int> object_move(std::move(object_copy));
    std::cout << "Object move: " << object_move.getMember() << std::endl;
    std::cout << "Object copy now: " << object_copy.getMember() << std::endl;

    // copy assignment operator call
    number = object; // number.operator=(object) - copy version
    std::cout << "Number now: " << number.getMember() << std::endl;

    // move assignment operator call
    number = std::move(object); // number.operator=(object) - move version
    std::cout << "Object now: " << object.getMember() << std::endl;

    return 0;
}
