// enum vs enum class

#include <iostream>

// anonymous
enum {
    ONE, TWO, THREE
};

// anonymous with variable
enum {
    SHORT = 8,
    LONG = 32,
    MIDDLE = 16
} word;

// regular
enum Colors {
    RED = 9, PINK, GREEN
};

// strongly-typed
enum class TrafficLights {
    RED, YELLOW, GREEN
};

// anonymous within a class
class Solar {
public:
    enum { EARTH = 88, MARS };
};

TrafficLights getColor() {
    return TrafficLights::RED;
}

int main() {
    // both calls correct
    std::cout << RED << '\n';
    std::cout << static_cast<int>(RED) << '\n';

    // only casting allowed
    std::cout << static_cast<int>(TrafficLights::RED) << '\n';
    //std::cout << traffic_lights::RED << '\n'; // ERROR
    
    if (getColor() == TrafficLights::RED)
        std::cout << "RED" << '\n';

    // new variable of enum
    Colors myColor;
    myColor = GREEN;
    std::cout << myColor << '\n';

    // already declared variable in enum
    word = MIDDLE;
    std::cout << word << '\n';

    // enum within a class
    std::cout << Solar::EARTH << '\n';
    
    return 0;
}
