// enum base example

#include <iostream>
#include <limits>

enum Colors { YELLOW = 10,
              ORANGE 
};
enum BigValue { VALUE = std::numeric_limits<long>::max() };
enum RgbColors : unsigned char {
    RED = 0x01,
    GREEN = 0x02,
    BLUE = 0x04,
    // BLACK = 0xFF + 1 // error: enumerator value 256 is outside
};

// the range of underlying type ‘unsigned char’
int main() {
    std::cout << sizeof(Colors) << std::endl;     // 4 - sizeof(int)
    std::cout << sizeof(BigValue) << std::endl;   // 8 - sizeof(long)
    std::cout << sizeof(RgbColors) << std::endl;  // 1 - sizeof(unsigned char)
    return 0;
}
