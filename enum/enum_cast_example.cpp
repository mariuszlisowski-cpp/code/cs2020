// enum cast example

#include <iostream>

enum class ErrorCode {
    lack_of_water = 333,
    too_much_load, // 334
    bearing_problem = 601,
    block_of_pump // 602
};

int main() {
    std::cout << static_cast<int>(ErrorCode::lack_of_water) << '\n';
    

    return 0;
}