// enum casting

#include <iostream>
#include <memory>

using namespace std;

enum class Error {
    WRONG_ID,
    WRONG_PASS
};

int main() {
    int i{};
    Error err = static_cast<Error>(i);
    int j = static_cast<int>(err);

    return 0;
}