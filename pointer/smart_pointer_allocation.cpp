// smart pointer allocation

#include <memory>

struct Msg {
    Msg(int i)
        : value(i) {}
    int value;
};

int main() {
    std::unique_ptr<Msg> ptrA(new Msg(1)); // function argument
    std::unique_ptr<Msg> ptrB{new Msg(2)}; // initializer list

    auto ptr1 = std::unique_ptr<Msg>(new Msg{ 5 });
    auto ptr2 = std::make_unique<Msg>(5); // equivalent to above
    
    return 0;
}
