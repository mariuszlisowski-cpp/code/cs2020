// cyclic dependencies solution

#include <memory>
struct Node {
    std::shared_ptr<Node> child;
    std::weak_ptr<Node> parent; // must we observer here
};
int main() {
    auto sharedA = std::shared_ptr<Node>(new Node);
    auto sharedB = std::shared_ptr<Node>(new Node);

    sharedA->child = sharedB;
    sharedB->parent = sharedA;
}
