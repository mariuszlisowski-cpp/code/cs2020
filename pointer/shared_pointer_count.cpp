// shared pointer count

#include <iostream>
#include <memory>

void foo(std::shared_ptr<int> ptr) {
    *ptr = 20;
    // ptr = nullptr; // ERROR
    std::cout << "Val: " << *ptr << " | use_count: " << ptr.use_count() << '\n';
    ptr = nullptr; // not needed any more (exitting a scope)
}

int main() {
    std::shared_ptr<int> number = std::make_shared<int>(10);

    std::cout << "Val: " << *number << " | use_count: " << number.use_count() << '\n';

    foo(number);

    std::cout << "Val: " << *number << " | use_count: " << number.use_count() << '\n';

    return 0;
}