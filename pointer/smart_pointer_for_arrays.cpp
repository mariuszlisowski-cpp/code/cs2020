// smart pointer for arrays

#include <memory>

struct MyData {};

void processPointer(MyData* md) {}
void processElement(MyData md) {}
void use(void) {
}

using Array = std::unique_ptr<MyData[]>;

int main() {
    Array arr{ new MyData[42] };
   
    processPointer(arr.get());
    processElement(arr[13]);
    
    
    return 0;
}