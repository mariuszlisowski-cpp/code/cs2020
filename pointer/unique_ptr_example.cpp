// unique pointer example

#include <iostream>
#include <memory>

using namespace std;

// test function
bool test(int ar[], int size) {
    // <body>
    return false;
}

// faulty function
int * function1(int size) {
    int * arr = new int[size];

    // memory leak
    if (!test(arr, size))
        return nullptr;
    // arr pointer still exists

    return arr;
}

// corrected function
int * function2(int size) {
    unique_ptr<int[]> arr(new int[size]); // must ponit to []

    if (!test(arr.get(), size))
        return nullptr;
    
    return arr.release();
}

int main() {
    unique_ptr<int> uptr1(new int);
    unique_ptr<int> uptr2;

    cout << (uptr1 != nullptr) << endl; // -> int
    cout << (uptr2 != nullptr) << endl; // null

    //uptr2 = uptr1; // pointer MUST BE UNIQUE
    uptr2 = move(uptr1);

    cout << (uptr1 != nullptr) << endl; // null
    cout << (uptr2 != nullptr) << endl; // -> int
    
    
    return 0;
}