// pointer const example

#include <iostream>

using namespace std;

void foo(int* p1, const int* p2, const int* p3, int* p4, const int* p5) {
    cout << *p1 << *p2 << *p3 << *p4 << *p5 << endl;
}

int main() {
    int var = 1; // regular var
    const int a = 9; // const var

    int* p1 = &var; // only to reg
    
    // pointer to const
    // const int = int const
    const int* p2 = &var; // to reg
    int const* p3 = &a; // to const

    // const pointer
    int* const p4 = &var;
    // cannot point elsewhere
    // p4 = nullptr; // ERROR

    // const pointer to const
    const int* const p5 = &a;
    // p5 = nullptr; // ERROR
    // p5 = &var; // ERROR

    foo(p1, p2, p3, p4, p5);

    return 0;
}
    
    