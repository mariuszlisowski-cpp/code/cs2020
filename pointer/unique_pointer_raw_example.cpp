// 

#include <iostream>
#include <memory>

class Resource { 
public:
    void use(const char* character) {
        std::cout << "Using resource. Passed " << *character << std::endl;
    };
};

int main() {
    char character{'Q'};
    char* charPtr = &character;
    
    // raw pointer
    Resource* rsc = nullptr;
    // initialization
    rsc = new Resource();
    // using
    rsc->use(charPtr);

    // unique pointer
    std::unique_ptr<Resource> rsource{}; // nullptr
    // initialization by external method
    rsource = std::make_unique<Resource>();
    // initialization by internal method
    rsource.reset(new Resource{});
    // using
    rsource->use(charPtr);

    return 0;
}