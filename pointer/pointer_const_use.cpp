// pointer const use

#include <iostream>

using namespace std;

// const pointer
void foo(int* const ptr) {
    //ptr = nullptr; // ERROR
    *ptr = 10;
}

// const pointer
void bar(int* const ptr) {
    //ptr = 20; // ERROR
    *ptr = 20;
}

int main() {
    int number = 5;
    int* pointer = &number;
    
    std::cout << number << '\n';
    foo(&number);
    
    std::cout << number << '\n';
    bar(pointer);
    
    std::cout << number << '\n';
    
    return 0;
}
    
    