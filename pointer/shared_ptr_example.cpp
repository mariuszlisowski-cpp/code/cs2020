// shared pointer example
// #use_count

#include <iostream>
#include <memory>

using namespace std;

int main() {
    shared_ptr<int> sptr1(new int);
    shared_ptr<int> sptr2;

    // pointer can be copied
    cout << (sptr1 != nullptr) << endl; // -> int
    cout << (sptr2 != nullptr) << endl; // null
    sptr2 = sptr1;
    cout << (sptr1 != nullptr) << endl; // -> int
    cout << (sptr2 != nullptr) << endl; // -> int

    // count reference use
    shared_ptr<int> ptr1(new int);
    shared_ptr<int> ptr2;
    cout << " ptr1: " << ptr1.use_count()
         << " ptr2: " << ptr2.use_count() << endl;
    ptr2 = ptr1;
    cout << " ptr1: " << ptr1.use_count()
         << " ptr2: " << ptr2.use_count() << endl;
    // another scope
    {
        shared_ptr<int> ptr3 = ptr2;
        cout << "  ptr2: " << ptr2.use_count()
             << "  ptr3: " << ptr3.use_count() << endl;
    }
    // scope exitted
    // pointer 3 deleted & pointer 42count decreased
    cout << " ptr1: " << ptr1.use_count()
         << " ptr2: " << ptr2.use_count() << endl;

    return 0;
}