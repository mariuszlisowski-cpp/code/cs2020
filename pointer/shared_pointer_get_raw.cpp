// shared pointer get raw

#include <iostream>
#include <memory>

// expects a raw pointer argument
void foo(int* const num) {
    (*num)++;
}

int main() {
    int a = 8;

    // raw pointer
    int* pt = &a;
    foo(pt);
    std::cout << *pt << std::endl;

    // shared pointer
    auto ptr = std::make_shared<int>(a);
    foo(ptr.get()); // uses a raw pointer
    std::cout << *ptr << std::endl;

    return 0;
}