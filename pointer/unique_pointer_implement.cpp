// unique pointer implementation 

#include <iostream>

namespace raisin {

template<typename T>
class unique_ptr {
public:
    unique_ptr(T* ptr) : ptr_(ptr) {}
    ~unique_ptr() { delete ptr_; }
    unique_ptr(const unique_ptr<T>&) = delete;
    unique_ptr(unique_ptr<T>&& ptr) noexcept : ptr_(ptr.ptr_) {
            ptr.ptr_ = nullptr;
    }

    unique_ptr<T>& operator=(const unique_ptr<T>&) = delete;
    unique_ptr<T>& operator=(unique_ptr<T>&& ptr) noexcept {
        // moving to itself?
        if (this != &ptr) {
            delete ptr_;
            ptr_ = ptr.ptr_;
            ptr.ptr_ = nullptr;
        }
        return *this;
    }
    T& operator*() const {
        return *ptr_;
    }

    T* get() const {
        return ptr_;
    }

    T* release() {
        T* temp = ptr_;
        ptr_ = nullptr;
        return temp;
    }

    void reset(T* ptr = nullptr) {
        delete ptr_;
        ptr_ = ptr;
    }

private:
    T* ptr_{};
};
}

int main() {
    raisin::unique_ptr ptrA{new int{ 5 }};

    std::cout << *ptrA << std::endl;
    std::cout << ptrA.get() << std::endl;
    auto raw = ptrA.release();
    delete raw;

    raisin::unique_ptr ptrB = std::move(ptrA);

    ptrA.reset(new int{ 9 });
    ptrA.reset();

    return 0;
}
