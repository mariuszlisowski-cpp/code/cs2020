// weak pointer usage
// ~ observes smart pointer

#include <iostream>
#include <memory>

struct Msg {
    int value;
};

void checkMe(const std::weak_ptr<Msg>& wp) {
    std::shared_ptr<Msg> p = wp.lock(); // returns pointer or nullptr
    if (p)
        std::cout << p->value << '\n';
    else
        std::cout << "Expired\n";
}

int main() {
    auto sp = std::shared_ptr<Msg>{ new Msg{ 10 } };
    auto wp = std::weak_ptr<Msg>{ sp }; // observe share pointer
    checkMe(wp);
    sp.reset();
    checkMe(wp);
}
