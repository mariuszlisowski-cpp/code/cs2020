// smart pointer used as raw

#include <memory>

void legacyInterface(int*) {} // still smart (no need to delete)
void deleteResource(int* p) { delete p; } // released before passing (must be deleted)
void referenceInterface(int&) {}

int main() {
    auto ptr = std::make_unique<int>(5);
    
    // pass as raw (still smart)
    legacyInterface(ptr.get());
    
    // releas and pass as raw (NOT smart anymore)
    deleteResource(ptr.release());
    
    // assign a new value
    ptr.reset(new int{ 10 });
    // or
    ptr = std::make_unique<int>(10);
    
    // pass a reference
    referenceInterface(*ptr);

    ptr.reset();  // ptr is a nullptr

    return 0;
}
