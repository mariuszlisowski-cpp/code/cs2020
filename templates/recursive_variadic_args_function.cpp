// recursive variadic args function 

#include <iostream>
#include <string>
#include <initializer_list>

template <typename T>
void recursiveFunc(T t) {
    std::cout << t << std::endl; // the last parameter used here
}

 // recursive variadic function
template <typename T, typename... Args>
void recursiveFunc(T t, Args... args) {
    std::cout << t << ' '; // all args except the last
    recursiveFunc(args...);  // unpack function parameters and use
}

// initializer list method
template <class T>
void initializerFunc(std::initializer_list<T> list) {
    for( auto el : list ) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {

    recursiveFunc(1, 2.5, 'a', "hi");

    std::string
        str1( "Hello" ),
        str2( "world" );
        
    initializerFunc( {10, 20, 30, 40 });
    initializerFunc( {str1, str2 } );
}
