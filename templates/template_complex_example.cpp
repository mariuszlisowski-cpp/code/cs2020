// template complex example

#include <complex>
#include <iostream>

template <typename T, typename K>
std::complex<K> makeComplex(T real, K imaginery) {
    return { real, imaginery };
}

int main() {
    std::complex<int> a = makeComplex(4, 5);
    std::complex<double> b = makeComplex(3.2, 2.2);
    std::complex<double> c = makeComplex(1, 5.1);
    // std::complex<int> c = makeComplex(1, 5.0);

    std::cout << c << std::endl;

    return 0;
}
