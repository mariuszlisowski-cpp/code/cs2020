//

#include <iostream>
#include <list>
#include <typeinfo>

template <class T>
void doSomething() {
    T value;
    std::cout << typeid(value).name() << std::endl;
}

int main() {
    doSomething<std::list<int>>();

    return 0;
}
