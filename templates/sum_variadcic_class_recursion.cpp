// sum in variadcic class recursion

#include <iostream>

template <int... Number>
struct Sum;

template <int Head, int... Tail>
struct Sum<Head, Tail...> {
    const static int RESULT = Head + Sum<Tail...>::RESULT;
};

template <>
struct Sum<> {
    const static int RESULT = 0;
};


int main() {
    constexpr auto value = Sum<1, 2, 3, 4, 5>::RESULT;  // = 15
    
    std::cout << value << std::endl;
    
    return 0;
}
