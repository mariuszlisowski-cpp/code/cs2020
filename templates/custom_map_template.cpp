// custom map template

#include <exception>
#include <iostream>
#include <map>
#include <unordered_map>

template <class Key, class Value>
class CustomMap {
public:
    void insert(Key&& key, Value&& value) {
        map_.insert(std::make_pair(std::forward<Key>(key),
                                   std::forward<Value>(value)));
    }

    Value& operator[](const Key& key) {
        auto it = map_.find(key);
        if (it != map_.end()) {
            return it->second;
        } else {
            auto pair = map_.insert({ key, Value{} });
            if (pair.second) {
                return pair.first->second;
            } else {
                throw std::out_of_range("Error! Not inserted.");
            }
        }
    }

    Value& at(const Key& key) {
        auto it = map_.find(key);
        if (it != map_.end()) {
            return it->second;
        } else {
            throw std::out_of_range("No such key!");
        }
    }

private:
    std::unordered_map<Key, Value> map_;
};

int main() {
    CustomMap<int, char> map;
    try {
        map.insert('b', 2);
        map['b'] = 3;                           // replaces key with value
        std::cout << (int)map['b'] << '\n';     // prints 'X'

        map['c'] = 9;                           // adds key with value
        std::cout << (int)map['c'] << '\n';     // prints 'Y'
        std::cout << (int)map.at('c') << '\n';  // prints 'Y'

        map.at('a');                            // throw std::out_of_range
    } catch (std::exception& ex) {
        std::cout << ex.what() << std::endl;
    }

    return 0;
}
