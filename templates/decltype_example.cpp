// decltype example
// ~ used in templates

#include <iostream>

int main() {
    int value = 9;
    decltype(value) sameTypeValue; // declared the same type

    sameTypeValue = 5.5; // implicit conversion to int
    
    std::cout << sameTypeValue << std::endl;

    return 0;
}
