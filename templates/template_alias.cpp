// template alias

#include <iostream>
#include <map>

template <typename T>
using StrKeyMap = std::map<std::string, T>;

struct Point {
    int x;
    int y;
};

main() {
    StrKeyMap<int> myMap; // std::map<std::string, int>
    StrKeyMap<Point> myMap; // std::map<std::string, Point>
    
    return 0;
}