// template specialization example 

#include <iostream>

template <int T>
struct isSmallPrime {
    static const bool value = false;
};
// or
// template <int T>
// struct isSmallPrime : std::false_type {};  // form <type_traits> lib

// specializations
template <>
struct isSmallPrime<2> {
    static const bool value = true;
};
// or
template <>
struct isSmallPrime<3> : std::true_type {}; // form <type_traits> lib

template <>
struct isSmallPrime<5> : std::true_type {};

template <>
struct isSmallPrime<7> : std::true_type {};

int main() {
    std::cout << std::boolalpha
              << isSmallPrime<0>::value << std::endl
              << isSmallPrime<1>::value << std::endl
              << isSmallPrime<2>::value << std::endl
              << isSmallPrime<3>::value << std::endl
              << isSmallPrime<4>::value << std::endl
              << isSmallPrime<5>::value << std::endl
              << isSmallPrime<6>::value << std::endl
              << isSmallPrime<7>::value << std::endl
              << isSmallPrime<8>::value << std::endl
              << isSmallPrime<9>::value << std::endl;

    return 0;
}
