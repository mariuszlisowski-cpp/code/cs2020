// type traits usage example

#include <iostream>

using namespace std;

// primary template
template <typename T>
struct is_int : std::false_type {};

// explicit specialization for T = int
template <>  
struct is_int<int> : std::true_type {};

int main() {
    std::cout << std::boolalpha
              << is_int<char>::value << std::endl;  // prints false
    std::cout << std::boolalpha
              << is_int<int>::value << std::endl;   // prints true

    return 0;
}
