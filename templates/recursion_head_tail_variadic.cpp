// variadic args head tail recursion

#include <iostream>

//
template <typename T>
void takeOneParameter(T head) {
    std::cout << head << std::endl;
}

// the last recursive call of the variadic template below
void variadicFoo() {}

template <class Head, class... Tail>
void variadicFoo(Head const& head, Tail const&... tail) {
    takeOneParameter(head);  // starts form head
    variadicFoo(tail...);  // takes the next parameter which becomes head
}

int main() {
    variadicFoo(9, 'a', 3.5, 'b', 8.4);  // variadicFoo<int, char, double, char, double>

    return 0;
}
