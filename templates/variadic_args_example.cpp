// variadic args example
// ~ not compiling

template <typename... Args>
void print_all(Args&&... args) {
    printMany(args...); // expands to printMany(arg1, arg2, ..., argN);
}

template <typename... Args>
void print_each(Args&&... args) {
    (printOne(args), ...);  // fold expression from C++17
    // expands to printOne(arg1), printOne(arg2), ..., printOne(argN);
}

int main() {
    print_all("Hello!", "Raisin");
    print_each("Hello!", "Raisin");

    return 0;
}
