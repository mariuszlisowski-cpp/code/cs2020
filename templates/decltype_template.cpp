// decltype template example

#include <complex>
#include <iostream>

template <typename Real, typename Imaginary>
auto makeComplex(Real real, Imaginary imaginary) {
    return std::complex<decltype(real)>{ real, imaginary }; // decltype used
}

int main() {
    std::complex<double> complexValue = makeComplex(3.1, 4.5);
    
    std::cout << complexValue.real()
              << " + "
              << complexValue.imag()
              << "i" << std::endl;

    return 0;
}
