//

#include <iostream>

template <class... Types>
struct NumOfArguments {
    const static unsigned value = sizeof...(Types);
};

int main() {
    constexpr auto argsNo = NumOfArguments<int, char, long>::value; // 3

    std::cout << argsNo << std::endl;    

    return 0;
}
