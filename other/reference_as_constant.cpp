// reference as constant 
// ~ reference cannot be null
// ~ reference cannot be assigned again
// ~ it's like const pointer once assigned

#include <iostream>

int main() {
    int val1 = 9;
    int val2 = 7;

    int& val1_ref = val1;
    val1_ref = val2; // val1_ref still alias for val1

    std::cout << val1_ref << std::endl;
    std::cout << val1 << std::endl; // val1 changed as well

    return 0;
}