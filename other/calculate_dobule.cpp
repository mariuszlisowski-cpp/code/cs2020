// calculate (dobule)

#include <iostream>
#include <string>

std::string calculate(const std::string& command, int first, int second) {
    double result;
    if (command == "add") {
        result = first + second;
    }
    else if (command == "substract") {
        result = first - second;
    }
    else if (command == "multiply") {
        result = first * second;
    }
    else if (command == "divide") {
        if (second) {
            result = static_cast<double>(first) / static_cast<double>(second);
        }
        else {
            return "Division by 0";
        }
    }
    else {
        return "Invalid data";
    }
    std::string str = std::to_string(result);
    // remove trailing zeroes
    str.erase(str.find_last_not_of('0') + 1, std::string::npos); 
     // remove dot
    str.erase(str.find_last_not_of('.') + 1, std::string::npos );
    return str;
}

int main() {

    std::cout << calculate("add", 10, 20) << std::endl;
    std::cout << calculate("substract", 10, 20) << std::endl;
    std::cout << calculate("multiply", 10, 20) << std::endl;
    std::cout << calculate("divide", 10, 20) << std::endl;
    
    std::cout << calculate("divide", 10, 0) << std::endl;
    std::cout << calculate("hello", 10, 20) << std::endl;

    return 0;
}