// generic lambda example
// ~ reusable lambdas with auto deduced arguments

#include <iostream>

int multiply(int first, int second) {
    return first * second;
}

int main() {
    int num{ 10 };

    // duduced arguments (here: closure type also deduced by a compiler)
    auto multiply_lambda = [&num](auto arg) { return multiply(arg, num); };

    std::cout << multiply_lambda(20) << '\n';  // argument here: 20

    return 0;
}
