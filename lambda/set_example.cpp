// set example
// ~ stores only unique elements (sorted)

#include <iostream>
#include <set>

int main() {
    // ascending (default)
    std::set<int, std::less<int>> ascending{ 5, 4, 3, 2, 1, 0, 6, 8, 7 };
    for (const auto el : ascending) {
        std::cout << el << ' ';
    }
    std::cout << '\n';

    // descending (greater)
    std::set<int, std::greater<int>> descending{ 5, 4, 3, 2, 1, 0, 6, 8, 7 };
    for (const auto el : descending) {
        std::cout << el << ' ';
    }
    std::cout << '\n';

    return 0;
}
