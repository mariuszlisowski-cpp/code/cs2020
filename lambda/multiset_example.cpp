// miultiset example
// ~ stores multiple same elements (sorted)

#include <iostream>
#include <set>

int main() {
    // ascending (default: less<T>)
    std::multiset<int> ascending{ 5, 4, 3, 2, 1, 0, 6, 8, 7, 1, 2, 3, 4, 5, 6 };
    for (const auto el : ascending) {
        std::cout << el << ' ';
    }
    std::cout << '\n';

    // descending (greater)
    std::multiset<int, std::greater<int>> descending{ 5, 4, 3, 2, 1, 0, 6, 8, 7, 1, 2, 3, 4, 5, 6 };
    for (const auto el : descending) {
        std::cout << el << ' ';
    }
    std::cout << '\n';

    return 0;
}
