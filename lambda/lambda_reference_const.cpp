// lambda reference const
// ~ not possible

#include <iostream>

int main() {
    int value = 5;

    // captures variable used as mutable (by reference)
    auto lamb = [&value]() { std::cout << value; };

    // const
    // auto lamb = [const &value]() { std::cout << value; }; // ERROR

    return 0;
}
