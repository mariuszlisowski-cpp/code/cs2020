// lambda mutable

#include <algorithm>
#include <iostream>
#include <vector>

int main() {
   
    std::vector<int> v(10);

    std::generate(v.begin(), v.end(),
                  [i = 0]() mutable {
                      return ++i;
                  });


    for (auto& e : v)
        std::cout << e << ' ';


    return 0;
}