// hash function
// ~ for the same data always returns the same index 

#include <iostream>
#include <string>

// very weak
size_t hash0(const std::string& str) {
    return str.size();
}

// weak
size_t hash1(const std::string& str) {
    size_t index = 0;
    for (size_t i = 0 ; i < str.size() ; ++i) {
        index += (int)str[i];
    }
    return index;
}

// stronger
size_t hash2(const std::string& str) {
    size_t index = 0;
    for (size_t i = 0 ; i < str.size() - 1 ; ++i) {
        index += ((int)str[i] * int(str[i + 1]) * (i + 5)) & (((int)str[i] + int(str[i + 1]) * i * i));
    }
    return index * str.size();
}

int main() {
    std::cout << hash0("kotek") << std::endl;
    std::cout << hash1("kotek") << std::endl;
    std::cout << hash2("kotek") << std::endl;

    return 0;
}