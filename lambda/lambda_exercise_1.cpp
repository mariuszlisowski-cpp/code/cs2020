// lambda exercise 1

#include <iostream>

int main() {
    // if argument is divisible by captured
    int captured = 6;
    [captured](int arg) { return arg % captured == 0; };

    // passing values to lambda (by value)
    [](int val1, int val2) { return val1 * val2; };

    // passing a string to lambda (by const ref)
    [](const std::string& str) { return "\"" + str + "\""; }; // "str"

    int i{1};
    auto lambda1 = [&i]() { return std::string(i++, '*'); };
    // or..
    auto lambda2 = [i{1}]() mutable { return std::string(i++, '*'); };

    // every lambda call produces longer string
    std::cout << lambda1();
    std::cout << '\n';
    std::cout << lambda1();
    std::cout << '\n';
    std::cout << lambda1();
    
    return 0;
}
