// lambda_exercise_2

#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> vec{ 1, 2, 3, 4 };

    // lambda in lambda
    // for_each accepts unary function (single argument; as each element)
    auto functor = [vec](){ std::for_each(vec.begin(), vec.end(),
        [](int el){
            std::cout << el << ' ' ;
        }); };

    // empty argument list
    functor();
    std::cout << '\n';

    // no arguments needed for lambda
    auto lambda = [](int arg){ std::cout << arg; };
    std::for_each(vec.begin(), vec.end(), lambda);

    return 0;
}