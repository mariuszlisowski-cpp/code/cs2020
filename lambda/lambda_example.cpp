// lambda example

#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    // simple lambda
    []() { std::cout << "H!\n"; };

    // erase even numbers using a functor
    // (remove if <not false>)
    std::vector<int> v{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    v.erase(std::remove_if(v.begin(),
                           v.end(),
                           [](int num) { return !(num % 2); }),
            v.end());

    for (const auto& el : v ) {
        std::cout << el << ' ';
    }

    // lambda type 'closure' known to copiler only
    auto print = [](int num) { std::cout << num << ' '; };

    return 0;
}
