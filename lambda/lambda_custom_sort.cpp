// lambda custom sort

#include <algorithm>
#include <iostream>
#include <vector>

template <class T>
void printCont(T arr) {
    std::for_each(arr.begin(),
                  arr.end(),
                  [](auto el) {
                      std::cout << el << ' ';
                  });
    std::cout << '\n';
}

// custom function
bool cmp(const int& a, const int& b) {
    return a > b;
}

int main() {
    std::vector<int> v{ 45, 56, 75, 32, 78, 99 };

    // ascending (default)
    std::sort(v.begin(), v.end());
    printCont(v);
 
    // descending (custom function)
    std::sort(v.begin(), v.end(), cmp);
    printCont(v);
 
    // descending (custom lambda)
    std::sort(v.begin(),
              v.end(),
              [](const int& a, const int& b) {
                  return a > b;
              });
    printCont(v);

    return 0;
}
