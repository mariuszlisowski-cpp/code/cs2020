// leak example
// ~ compile with -fsanitize=leak -g
//   or -fsanitize=address -g
// ~ when using valgrind do not use above flags

#include <iostream>

int main() {
    int* ptr = new int(5);

    std::cout << *ptr << std::endl;

    // delete ptr; // shoutd be uncommented :)

    return 0;
}