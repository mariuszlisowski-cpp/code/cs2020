// RAII file operations
// Coders School versrion

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>

// custom exception
class FileOpeningError : public std::runtime_error {
public:
    FileOpeningError(std::string filename)
        : std::runtime_error("File " + filename + " not found") {}
};
 
class FileHandler {
public:
    // constructor accepting strings
    FileHandler(std::string filename) :
        fp_(std::fopen(filename.c_str(), "r")) {
            if (!fp_) {
                throw FileOpeningError(filename);
            }
        }
    // constructor accepting c-strings
    FileHandler(const char* filename) :
        fp_(std::fopen(filename, "r")) {
            if (!fp_) {
                throw FileOpeningError(filename);
            }
        }
    ~FileHandler() {
        std::fclose(fp_);
    }

    friend std::ostream& operator<<(std::ostream& out, FileHandler& fh) {
        int c; // not 'char' to handle EOF
        // C I/O file
        while ((c = std::fgetc(fh.fp_)) != EOF) {
            out << static_cast<char>(c);
        }

        if (std::ferror(fh.fp_)) {
            throw std::runtime_error("I/O error while reading");
        } else if (std::feof(fh.fp_)) {
            out << "\nEnd of file reached successfully\n";
        }

        return out;
    }
private:
    FILE* fp_;
};

int main() {
    try {
        FileHandler fileHandler(std::string("file_operations_CS.cpp")); // not a C string
        std::cout << fileHandler;
    } catch (FileOpeningError& foe) {
        std::cerr << foe.what() << std::endl;
    } catch (std::runtime_error& re) {
        std::cerr << re.what() << std::endl;
    }

    return 0;
}
