// stack overflow

double* foo() {
    // stack overflow    
    double stack_array[1048576]; // segmentation fault
    
    // heap used instead
    double* heap_array = new double[1048576]; // no problem here

    return heap_array;
}

int main() {
    double * array = foo();

    delete array;

    return 0;
}
