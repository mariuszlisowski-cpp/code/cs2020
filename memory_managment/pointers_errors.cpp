// commont errors

#include <iostream>

// OUT OF BOUNDS MEMORY ACCESS
void out_of_bounds_memory() {
    const auto size = 10;
    int* dynamicArray = new int[size];
    for (int i = 0; i <= size; ++i) {  // <= instead of <
        *(dynamicArray + i) = i * 10;
    }
    for (int i = 0; i <= size; ++i) {  // <= instead of <
        std::cout << dynamicArray[i] << '\n';
    }
    delete[] dynamicArray;
}

// DANGLING POINTER
struct Msg {
    int value{ 100 };
};
class DanglingPointer {
    void processMsg(Msg* msg) {
        std::cout << msg->value << '\n';
    }
    void dangling() {
        Msg* m = new Msg();
        delete m;
        processMsg(m);  // accessing non-existing pointer
    }
};

// DOUBLE DELETE
class Meseg {};
class DoubleDelete {
    void processMeseg(Meseg* msg) {
        // ...
        delete msg;  // first delete
    }
    void deleting() {
        Meseg* m = new Meseg{};
        processMeseg(m);
        delete m;  // second delete
    }
};

// NULL POINTER DEREFERENCE
void dereference() {
    int* p = new int{ 10 };
    delete p;
    p = nullptr;
    std::cout << *p << '\n';  // pointer is null now
}

// FREEING STACK ALLOCATED BLOCKS
class Mesg {};
class StackAllocated {
    void processMsg(Mesg* msg) {
        // ...
        delete msg;  // deleting stack allocated object
    }
    void process() {
        Mesg m;
        processMsg(&m);
    }
};

// FREEING A PORTION OF A DYNAMIC BLOCK
void portion() {
    constexpr auto size = 4u;
    int* array = new int[size]{ 1, 2, 3, 4 };
    delete array;  // freeing first element instead the whole array (delete[])
}

// MEMORY LEAK
void leak() {
    int* p = new int{ 10 }; // memory lost 
    p = new int{ 20 }; // pointer reused (pointing to new address)
    std::cout << *p << '\n';
    delete p; // deleting value 20
}

int main() {
    return 0;
}