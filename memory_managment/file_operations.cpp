// RAII file operations

#include <fstream>
#include <fstream>
#include <iostream>
#include <ostream>
#include <stdexcept>
#include <string>

class FileHandler {
public:
    FileHandler(std::string filename) {
        file_.open(filename);
        if (!file_.is_open()) {
            throw std::invalid_argument("File not found!");
        }
    }
    ~FileHandler() {
        file_.close();
    }
    
    friend std::ostream& operator<<(std::ostream&, FileHandler&);

private:
    std::ifstream file_;
};

std::ostream& operator<<(std::ostream& out, FileHandler& fileHandler) {
    std::string line;
    if (fileHandler.file_.is_open()) {
        while (!fileHandler.file_.eof()) {
            std::getline(fileHandler.file_, line);
            out << line << std::endl;
        }
    }

    return out;
}

int main() {
    try {
        FileHandler file("file_operations.cpp");
        std::cout << file;
    } catch (const std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
