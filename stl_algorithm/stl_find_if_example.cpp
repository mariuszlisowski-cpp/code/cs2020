// stl find_if example

#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5 };

    auto found = std::find_if(std::begin(v), std::end(v),
                              [](const auto& el) {
                                  return el == 4;
                              });

    if (found != v.end()) {
        std::cout << *found << '\n';
    }                            
    
    return 0;
}
