// find equal adjacent in a vector
// #find_if

#include <algorithm>
#include <iostream>
#include <vector>

template <class T>
std::vector<std::pair<T, size_t>>
adjacent_count(const std::vector<T>& vec) {
    std::vector<std::pair<T, size_t>> pairs;

    for (auto beg = vec.begin(), end = vec.end(); beg != end;) {
        // iterator to the first not equal
        const auto diff = std::find_if(beg, end,
                                       [beg](const auto& el) {
                                           return *beg != el;
                                       });
        pairs.emplace_back(*beg, std::distance(beg, diff));
        beg = diff;
    }

    return pairs;
}

template <class TFirst, class TSecond>
void printVecPairs(std::vector<std::pair<TFirst, TSecond>> pairs) {
    std::for_each(pairs.begin(),
                  pairs.end(),
                  [](const auto& el) {
                      std::cout << el.first << " : "
                                << el.second << '\n';
                  });
}

int main() {
    std::vector v{ 'a', 'a', 'b', 'c', 'c', 'c', 'a', 'a', 'a' };

    auto vecPairs = adjacent_count(v);
    printVecPairs(vecPairs);

    return 0;
}
