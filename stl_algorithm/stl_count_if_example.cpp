// stl count_if example

#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> vec{ 1, 2, 3, 4, 5, 1, 1, 1, 6, 7 };
    
    // amount of occurences of 1
    std::cout << std::count(begin(vec), end(vec), 1) << '\n';

    // amount of numbers divisible by 3 (here: 3 & 6)
    auto counter = std::count_if(begin(vec), end(vec),
                                 [](const auto& el) {
                                     return el % 3 == 0;
                                 });

    std::cout << counter << '\n';

    return 0;
}
