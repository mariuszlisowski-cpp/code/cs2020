// stl modifying algorithms
// #copy #copy_if #copy_n #copy_backward
// #move #move_backward
// #transform
// #fill #generate
// #swap_ranges
// #replace #replace_if #replace_copy #replace_copy_if
// #remove #remove_if #remove_copy #remove_copy_if
// #unique #unique_copy
// #reverse #reverse_copy
// #rotate #rotate_copy
// #next_permutation #prev_permutation
// #shuffle #random_shuffle

#include <algorithm>
#include <iostream>
#include <random>
#include <vector>

constexpr int MIN = 1;
constexpr int MAX = 9;

template <class T>
void display(T& arr) {
    for (const auto& el : arr) {
        std::cout << el << ' ';
    }
    std::cout << '\n';
}

int generateRandom() {
    static std::random_device rd;
    static std::mt19937 generator(rd());

    std::uniform_int_distribution<int> distr(MIN, MAX);
    return distr(generator);
}

int main() {
    std::vector<int> v{ 9, 60, 70, 8, 45, 87, 90 };
    std::vector<int> vec(15), vep(20), ver(15);
    display(v);

    std::vector<int>::iterator itr;
    std::pair<std::vector<int>::iterator, std::vector<int>::iterator> itr_pair;

    std::cout << "/////////////////////////////////////////////////////////\n";
    /// VALUE CHANGING
    std::cout << "/////////////////////////////////////////////////////////\n";
    // copying
    std::copy(v.begin(), v.end(),  // source
              vec.begin());        // destination
    display(vec);
    std::copy_if(v.begin(), v.end(),               // source
                 vec.begin() + 10,                 // destination
                 [](int el) { return el > 80; });  // condition
    display(vec);
    std::copy_n(v.begin(), 2, vec.begin() + 13);  // here: n = 2
    display(vec);
    std::copy_backward(v.begin(), v.end(),  // source
                       vec.end());          // destination (here: end)
    display(vec);
    std::cout << "/////////////////////////////////////////////////////////\n";
    // merging
    std::merge(std::begin(v), std::end(v),          // source
               std::begin(vec), std::end(vec),      // source
               std::back_inserter(ver));            // destination
               
    std::cout << "/////////////////////////////////////////////////////////\n";
    // moving
    std::move(vec.begin(), vec.end(),  //source
              vep.begin());            // destination
    display(vep);
    std::move_backward(vec.begin(), vec.end(),  //source
                       vep.end());              // destination (here: end)
    display(vep);

    std::cout << "/////////////////////////////////////////////////////////\n";
    vec.clear();
    vec.resize(15, 2);  // size 15
    vep.clear();
    vep.resize(15);
    display(vec);
    // tranforming
    std::transform(vec.begin(), vec.end(),          // source
                   vep.begin(),                     // destination
                   [](int el) { return el + 5; });  // operation
    display(vep);
    std::transform(vec.begin(), vec.end(),                       // source #1
                   vep.begin(),                                  // source #2
                   ver.begin(),                                  // destination
                   [](int elc, int elp) { return elc + elp; });  // operation
    display(ver);

    std::cout << "/////////////////////////////////////////////////////////\n";
    // filling
    std::fill(ver.begin(), ver.end(), 3);
    std::fill_n(vec.begin(), 10, 8);  // fill 10 elements from begin()
    // generating
    std::generate(vec.begin(), vec.end(), generateRandom);
    std::generate_n(ver.begin(), 5, generateRandom);  // generate 5 elements
    display(ver);
    display(vec);

    std::cout << "/////////////////////////////////////////////////////////\n";
    // swapping
    std::swap_ranges(ver.begin(), ver.end(),
                     vec.begin());
    display(vec);
    display(ver);

    std::cout << "/////////////////////////////////////////////////////////\n";
    // replacing
    std::replace(vec.begin(), vec.end(),  // range
                 8,                       // old value
                 99);                     // new value
    std::replace_if(
        vec.begin(), vec.end(),          // range
        [](int el) { return el > 80; },  // old value condition
        9);                              // new value
    std::replace_copy(
        ver.begin(), ver.end(),  // source
        vec.begin(),             // destination
        8,                       // old value
        77);                     // new value
    display(vec);
    std::replace_copy_if(
        ver.begin(), ver.end(),          // source
        vec.begin(),                     // destination
        [](int el) { return el > 80; },  // old value condition
        77);                             // new value
    display(vec);

    std::cout << "/////////////////////////////////////////////////////////\n";
    // removing
    std::remove(vec.begin(), vec.end(), 3);  // all occurences of 3
    std::remove_if(vec.begin(), vec.end(),
                   [](int el) { return el > 80; });  // all above 80
    std::remove_copy(vec.begin(), vec.end(),         // source unchanged
                     vep.begin(),                    // unremoved items
                     6);                             // value to remove
    std::remove_copy_if(
        vec.begin(), vec.end(),           // source unchanged
        vep.begin(),                      // unremoved items
        [](int el) { return el > 80; });  // occurences above 80 to remove
    // removing consecutive equal
    std::unique(vec.begin(), vec.end());      // sort if removing all repeating
    std::unique(vec.begin(), vec.end(),       // remove elements whose previous
                std::less<int>());            // ..element is less than itself
    std::unique_copy(vec.begin(), vec.end(),  // remove consecutive elements..
                     vep.begin());            // ..copy the result
    std::unique_copy(vec.begin(), vec.end(),  // source unchanged
                     vep.begin(),             // ..copy the result
                     std::less<int>());       //

    std::cout << "/////////////////////////////////////////////////////////\n";
    /// ORDER CHANGING
    std::cout << "/////////////////////////////////////////////////////////\n";
    // reversing
    std::reverse(vec.begin(), vec.end());
    std::reverse_copy(vec.begin(), vec.end(),  // source unchanged
                      vep.begin());            // reversed range destination

    std::cout << "/////////////////////////////////////////////////////////\n";
    // rotating
    std::rotate(vec.begin(), vec.begin() + 3, vec.end());
    std::rotate_copy(vec.begin(), vec.begin() + 3, vec.end(),
                     vep.begin());  // rotated data destination

    std::cout << "/////////////////////////////////////////////////////////\n";
    // permuting
    std::next_permutation(vec.begin(), vec.end());  // lexicographically greater
    std::prev_permutation(vec.begin(), vec.end());  // lexicographically smaller

    std::cout << "/////////////////////////////////////////////////////////\n";
    // shuffling
    std::random_shuffle(vec.begin(), vec.end());  // swap randomly within range
    std::shuffle(vec.begin(), vec.end(),
                 std::default_random_engine());  // default generator
    std::shuffle(vec.begin(), vec.end(),
                 std::mt19937{ std::random_device{}() });  // better generator

    return 0;
}
