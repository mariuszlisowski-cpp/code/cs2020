// lexicographical compare

#include <functional>
#include <iostream>
#include <set>
#include <string>

using std::cout;
using std::multiset;
using std::string;

int main() {
    auto words = { "Abc", "abc", "boom", "great", "abc", "Boot", "Alice" };

    // using buid-in comparator
    // template <class Key, class Compare = std::less<Key>>
    multiset<string> ms;

    // sorting
    for (const auto& word : words)
        ms.insert(word);

    // displaying
    for (const auto& el : ms)
        cout << el << '\n';

    return 0;
}
