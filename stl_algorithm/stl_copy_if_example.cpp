// stl copy_if example

#include <algorithm>
#include <iostream>
#include <vector>

template <class T>
void print(T& arr) {
    for (const auto& e : arr) {
        std::cout << e << ' ';
    }
    std::cout << '\n';
}

int main() {
    std::vector<int> vec{ 1, 2, 3, 4, 5 };
    std::array<int, 5> arr;

    std::copy(begin(vec), end(vec), begin(arr));
    print(arr);

    std::vector<int> vec2(3);
    std::copy_if(begin(vec), end(vec), begin(vec2),
                 [](auto num) {
                     return num % 2;          // odd
                    //  return num % 2 == 1;  // odd
                    //  return !(num % 2);    // even
                    //  return num % 2 == 0;  // even
                 });
    print(vec2);

    return 0;
}
