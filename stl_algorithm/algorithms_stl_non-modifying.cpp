// stl non-modifying algorithms
// #count #count_if
// #min_element #max_element #minmax_element
// #find #find_if #find_if_not
// #search #find_end
// #search_n #adjacent_find
// #equal #is_permutation
// #mismatch #lexicographical_compare

#include <algorithm>
#include <iostream>
#include <vector>

template <class T>
void display(T& arr) {
    for (const auto& el : arr) {
        std::cout << el << ' ';
    }
    std::cout << '\n';
}

int main() {
    std::vector<int> v{ 9, 60, 90, 8, 45, 87, 90, 69, 69, 55, 7 };
    std::vector<int>::iterator itr;
    std::pair<std::vector<int>::iterator, std::vector<int>::iterator> itr_pair;
    display(v);

    std::cout << "/////////////////////////////////////////////////////////\n";
    /// counting
    // count
    long long int n = std::count(v.begin(), v.end(), 69);
    // count (generalized form)
    long long int m = std::count_if(v.begin(), v.end(),
                                    [](int el) {
                                        return el < 10;
                                    });  // all less than..
    std::cout << "Counts of 69: " << n << '\n';
    std::cout << "Counts of less than 10: " << m << '\n';

    std::cout << "/////////////////////////////////////////////////////////\n";
    /// max & min
    // max (also generalized form)
    itr = std::max_element(v.begin(), v.end());
    std::cout << "Max of range: " << *itr << '\n';
    // min (generalized form)
    itr = std::min_element(v.begin(), v.end(),
                           [](int x, int y) {
                               return (x % 10) < (y % 10);
                           });  // look at the last digits
    std::cout << "With smallest last digit: " << *itr << '\n';

    // minmax (with generalized form)
    itr_pair = std::minmax_element(v.begin(), v.end());
    std::cout << "Min & max: "
              << *itr_pair.first << ", "
              << *itr_pair.second << '\n';

    // minmax (custom comparision function)
    itr_pair = std::minmax_element(v.begin(), v.end(),
                                   [](int x, int y) {
                                       return (x % 10) < (y % 10);
                                   });  // look at the last digits
    std::cout << "With the last digit min & max: "
              << *itr_pair.first << ", "
              << *itr_pair.second << '\n';

    std::cout << "/////////////////////////////////////////////////////////\n";
    /// linear searching (for unsorted data)
    // find (with generalized forms)
    itr = std::find(v.begin(), v.end(), 55);  // the first occurence
    itr = std::find_if(v.begin(), v.end(),
                       [](int el) { return el > 80; });
    itr = std::find_if_not(v.begin(), v.end(),
                       [](int el) { return el > 80; });
    std::cout << "First not greater than 80: " << *itr << '\n';
    // search (also generalized forms)
    itr = std::search_n(v.begin(), v.end(), 2, 69);  // consecutive 2 items of 69
    // search subrange (also generalized forms)
    std::vector<int> sub{ 45, 87, 90 };
    itr = std::search(v.begin(), v.end(), sub.begin(), sub.end());    // first
    itr = std::find_end(v.begin(), v.end(), sub.begin(), sub.end());  // last
    // search any of (with generalized form)
    std::vector<int> items{ 87, 69 };  // any of them
    itr = std::find_first_of(v.begin(), v.end(), items.begin(), items.end(),
                             [](int x, int y) {
                                 return x == y * 4;
                             });  // returns v.end() here
    itr = std::find_first_of(v.begin(), v.end(), items.begin(), items.end());
    std::cout << "Any of: " << *itr << '\n';
    // search adjacent (with generalized form)
    itr = std::adjacent_find(v.begin(), v.end(),
                             [](int x, int y) {
                                 return x == y * 4;
                             });  // returns v.end() here
    itr = std::adjacent_find(v.begin(), v.end());
    std::cout << "Adjacent elements: " << *itr << '\n';

    std::cout << "///////////!//////////////////////////////////////////////\n";
    /// comparing ranges
    // equality (also generalized forms)
    if (!std::equal(v.begin(), v.begin() + v.size() / 2, v.rbegin())) {
        std::cout << "Not palindrome\n";
    }
    if (!std::equal(v.begin(), v.end(), items.begin(), items.end())) {
        std::cout << "Not the same\n";
    }
    // permutations (also generalized forms)
    std::vector<int> vec{ 7, 60, 90, 8, 45, 87, 90, 69, 69, 55, 9 };
    if (std::is_permutation(v.begin(), v.end(), vec.begin(), vec.end())) {
        std::cout << "Have same items in different order\n";
    }
    // differences (also generalized forms)
    itr_pair = std::mismatch(v.begin(), v.end(), vec.begin());
    std::cout << "Differences in: "
              << *itr_pair.first << " & "   // difference in first container
              << *itr_pair.second << '\n';  // difference in second container
    // lexicographical compare (also generalized forms)
    // one-by-one less than.. comparison
    bool boolean = std::lexicographical_compare(v.begin(), v.end(),
                                                vec.begin(), vec.end());

    std::cout << "/////////////////////////////////////////////////////////\n";
    /// checking attributes
    // sorted (also generalized forms)
    boolean = std::is_sorted(v.begin(), v.end());
    itr = std::is_sorted_until(v.begin(), v.end());
    // partitioned
    boolean = std::is_partitioned(v.begin(), v.end(),
                                  [](int x) { return x > 80; });
    // heap (also generalized forms)
    boolean = std::is_heap(v.begin(), v.end());
    itr = std::is_heap_until(v.begin(), v.end());
    // all (match the condition in labmda)
    boolean = std::all_of(v.begin(), v.end(), [](int el) { return el > 80; });
    // any (match the condition in labmda)
    boolean = std::any_of(v.begin(), v.end(), [](int el) { return el > 80; });
    // none (match the condition in labmda)
    boolean = std::none_of(v.begin(), v.end(), [](int el) { return el > 80; });

    if (boolean)
        exit(0);

    return 0;
}
