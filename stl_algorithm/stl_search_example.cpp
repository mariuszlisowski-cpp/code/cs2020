// stl search example

#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> vec1{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    std::vector<int> vec2{ 4, 5, 6 };

    std::vector<int>::iterator itr;
    itr = std::search(begin(vec1), end(vec1),
                             begin(vec2), end(vec2));

    if (itr != vec1.end()) {
        std::cout << "Subrange of vec2 inside vec1 starts at index: "
                  << std::distance(vec1.begin(), itr) << '\n'
                  << "and its value is: " << *itr << '\n';
    }

    return 0;
}
