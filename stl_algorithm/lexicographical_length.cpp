// lexicographical & length compare
// #multiset #Compare #operator() #struct

#include <iostream>
#include <set>
#include <string>

using std::cout;
using std::multiset;
using std::string;

// old solution
struct Compare {
    inline bool operator()(const string& s1, const string& s2) const {
        // compare by length
        if (s1.size() != s2.size())
            return s1.size() < s2.size();
        // then lexicographically
        else
            return s1 < s2;
    }
};

int main() {
    auto words = { "Abc", "abc", "boom", "great", "abc", "Boot", "Alice" };

    // using custom comparator
    multiset<string, Compare> ms;

    // sorting
    for (const auto& word : words)
        ms.emplace(word);

    // displaying
    for (const auto& el : ms)
        cout << el << '\n';

    return 0;
}
