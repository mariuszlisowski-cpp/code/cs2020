// lexicographical & length compare
// #multiset #Compare #operator() #struct

#include <iostream>
#include <set>
#include <string>

using std::cout;
using std::multiset;
using std::string;

int main() {
    auto words = { "Abc", "abc", "boom", "great", "abc", "Boot", "Alice" };

    auto cmp = [](const string& s1, const string& s2) {
        if (s1.size() != s2.size())
            return s1.size() < s2.size();
        else
            return s1 < s2;
    };

    // using custom comparator
    // C++20
    multiset<string, decltype(cmp)> ms;
    // C++11
    // multiset<string, decltype(cmp)> ms(cmp);

    // sorting
    for (const auto& word : words)
        ms.emplace(word);

    // displaying
    for (const auto& el : ms)
        cout << el << '\n';

    return 0;
}
