// stl exercise 1
// #generate #copy #merge #assign()

#include <algorithm>
#include <deque>
#include <iostream>
#include <numeric>  // iota()
#include <list>
#include <vector>

int oddGenerator(int i) {
    return ++i;
}

template <class T>
void display(const T& arr) {
    std::for_each(arr.begin(), arr.end(),
                  [](int el) {
                      std::cout << el << ' ';
                  });
    std::cout << '\n';
}

int main() {
    std::vector<int> v(8);
    std::vector<int> w(8);

    // sequence starting from 1
    std::iota(v.begin(), v.end(), 1);
    display(v);

    // fill with odd numbers [1, 15]
    int i{ -1 };
    std::generate(v.begin(), v.end(),
                  [&i]() {
                      return i += 2;
                  });
    // or..
    display(v);

    // reverse without 'reverse' algorithm
    std::vector<int> vec;
    std::vector<int> vep;
    std::copy(v.rbegin(), v.rend(), std::back_inserter(vec));
    // or..
    vep.assign(v.rbegin(), v.rend());
    display(vec);
    display(vep);

    // copy vector to list
    std::list<int> l;
    std::copy(v.begin(), v.end(), std::back_inserter(l));
    display(l);

    // fill with even numbers [0, 14]
    int j{ -2 };
    std::generate(v.begin(), v.end(),
                  [&j]() {
                      return j += 2;
                  });
    display(v);

    // join list & vector with odd & even numbers alternately
    std::deque<int> d;
    std::merge(v.begin(), v.end(),
               l.begin(), l.end(),
               std::back_inserter(d));
    display(d);

    // join list & vector with odd & even numbers sequentially
    std::deque<int> dq;
    std::merge(v.begin(), v.end(),
               l.begin(), l.end(),
               std::back_inserter(dq),
               [](int elv, int ell) {
                   if (ell % 2) {
                       return elv;
                   } else {
                       return ell;
                   }
               });
    display(dq);

    return 0;
}
