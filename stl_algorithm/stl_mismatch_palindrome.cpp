// stl mismatch palindrome

#include <iostream>
#include <algorithm>

bool isPalindrome(std::string str) {
    return (std::mismatch(str.begin(),
                          str.end(),
                          str.rbegin()))
               .first == str.end();
}

void test(const std::string& s) {
    std::cout << "\"" << s << "\" "
              << (isPalindrome(s) ? "is" : "is not")
              << " a palindrome\n";
}

int main() {
    test("rotator");    

}
