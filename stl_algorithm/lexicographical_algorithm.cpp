// lexicographical algorithm

#include <algorithm>  // lexicographical_compare
#include <iostream>
#include <string>

int main() {
    std::string s1{ "Alice" };
    std::string s2{ "Alica" };

    if (lexicographical_compare(s1.begin(), s1.end(), s2.begin(), s2.end())) {
        std::cout << s1 << " is lexicographically less than " << s2;
    } else {
        std::cout << s1 << " is NOT lexicographically less than " << s2;
    }
}
