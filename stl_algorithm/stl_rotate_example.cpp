// rotate example

#include <algorithm>
#include <iostream>
#include <vector>

bool changePosition(std::vector<int>& vec, int value, int newPosition) {
    if (newPosition >= vec.size()) {
        return false;
    }
    auto it = std::find(std::begin(vec), std::end(vec), value);
    if (it == vec.end()) {
        return false;
    }

    auto valuePosition = std::distance(vec.begin(), it);
    if (valuePosition < newPosition) {
        std::cout << "First in range: " << *it << std::endl;
        std::cout << "Last after range: " << *(vec.begin() + newPosition + 1) << std::endl;
        std::cout << "New beginging: " << *(it + 1) << std::endl;
        std::rotate(it, it + 1, vec.begin() + newPosition + 1); // first, new beginning, last after
    } else if (valuePosition > newPosition) {
        std::cout << "First in range: " << *(vec.begin() + newPosition) << std::endl;
        std::cout << "Last after range: " << *(it + 1) << std::endl;
        std::cout << "New beginging: " << *it << std::endl;
        std::rotate(vec.begin() + newPosition, it, it + 1); // first, new beginning, last after
    } else {
        return false;  // bothe are equal (no rotation)
    }

    return true;
}

void printVector(std::vector<int> v) {
    for (auto&& i : v) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

int main() {
    std::vector<int> initial{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    // inserting value 2 at position 8
    std::vector<int> v{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    if (changePosition(v, 2, 8)) {; // searched value, new position
        std::cout << "result:  ";
        printVector(v);
    }
    std::cout << "initial: ";
    printVector(initial);

    std::cout << "----------------------------" << std::endl;

    // inserting value 8 at position 2
    std::vector<int> w{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    if (changePosition(w, 8, 2)) {; // searched value, new position
        std::cout << "result:  ";
        printVector(w);
    }
    std::cout << "initial: ";
    printVector(initial);

    return 0;
}
