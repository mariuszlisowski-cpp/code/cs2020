// stl equal example

#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> vec1{ 1, 2, 4, 2, 5 };
    std::vector<int> vec2{ 1, 2, 4, 2, 5, 8, 9 };

    // compare till the end of the first range
    std::cout << std::boolalpha << "EQUAL?: "
              << std::equal(begin(vec1), end(vec1), begin(vec2))
              << '\n'; // same content till the end of the first range (true)

    // compare till the end of the first range
    std::cout << std::boolalpha << "EQUAL?: "
              << std::equal(begin(vec2), end(vec2), begin(vec1))
              << '\n'; // second range (vec1) shorter (false)

    // compare whole ranges
    std::cout << std::boolalpha << "EQUAL?: "
              << std::equal(begin(vec1), end(vec1),
                            begin(vec2), std::next(vec2.begin(), 5))
              << '\n'; // same sizes of ranges & same content (true)

    return 0;
}
