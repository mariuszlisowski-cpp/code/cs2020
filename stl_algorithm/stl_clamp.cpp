// stl clamp

#include <algorithm>
#include <iostream>

int main() {
    for (int i = 0; i < 15; ++i) {
        int num = std::clamp(i, 5, 10); // (val, min, max)
        std::cout << num << ' ';
    }

    return 0;
}