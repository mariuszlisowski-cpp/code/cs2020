// stringstream class example 

#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

int main() {
    std::string str {"Ala ma kota, kot ma Ale, a sierotka ma rysia"};
    std::istringstream iss(str);

    std::vector<std::string> vec {
        std::istream_iterator<std::string>(iss), {}};
    std::copy(begin(vec), end(vec),
              std::ostream_iterator<std::string>(std::cout, "\n"));
    
    return 0;
}
