// file write & read example 

#include <fstream>
#include <iostream>
#include <string>

enum class FileMode {
    TEXT,
    BINARY
};

// trunc    -> wymazuje wszystko w pliku co było do tej pory
// in       -> tryb do odczytu
// out      -> tryb do zapisu
// ate      -> ustawia seek na końcu pliku
// app      -> ustawia seek na końcu pliku przed zapisaniem do niego danych,
//             jednym słowem "doklejamy" nowe wartości na końcu pliku
// binary   -> zapis/odczyt w trybie binarnym

void fileWrite(std::string filename, FileMode mode) {
    bool error = false;
    std::fstream file;
    if (mode == FileMode::TEXT) {
        file.open(filename, file.out | file.app); // write & append
        if (file.is_open()) {
            file << "Anything written to a file\n";
            file.close();
        } else {
            error = true;
        }
    } else {
        file.open(filename, file.out | file.app | file.binary);
        if (file.is_open()) {
            char buffer[] {"abcdefghij"};
            file.seekp(0); // put at the beginning
            file.write(buffer, sizeof(buffer));
            file.close();
        } else {
            error = true;
        } 
    }
    if (error) {
        std::cerr << "File open error!" << std::endl;
    }
}

void fileRead(std::string filename, FileMode mode) {
    bool error = false;
    std::fstream file;
    if (mode == FileMode::TEXT) {
        file.open(filename, file.in); // read
        if (file.is_open()) {
            std::string line;
            while (!getline(file, line, '\n').eof()) {
                std::cout << line << '\n';
            }
            file.close();
        } else {
            error = true;
        }
    } else {
        file.open(filename, file.in | file.binary);
        if (file.is_open()) {
            char buffer[10]; // EXAMPLE ONLY
            file.seekg(0);  // get from the beginning
            file.read(buffer, 10);
            std::cout << buffer;
            file.close();
        } else {
            error = true;
        }
    }
    if (error) {
        std::cerr << "File open error!" << std::endl;
    }
}

void fileReadWrite(std::string filename) {
    std::fstream file(filename, file.in | file.out | file.binary);

    char buffer[] {"abcdefghij"};
    char output[sizeof(buffer)];

    if (file.is_open()) {
        // writing at the beginning
        file.seekp(0);
        file.write(buffer, sizeof(buffer));
        // reading from the beginning
        file.seekg(0);
        file.read(output, sizeof(output));
        // closing
        file.close();
    }
    std::cout << buffer;
}

int main() {
    fileWrite("text", FileMode::TEXT);
    fileWrite("binary", FileMode::BINARY);

    fileRead("text", FileMode::TEXT);
    fileRead("binary", FileMode::BINARY);

    fileReadWrite("test.binary");

    return 0;
}
