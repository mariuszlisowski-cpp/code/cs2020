// stringstream conversion to string

#include <iostream>
#include <sstream>
#include <string>

int main() {
    std::stringstream ss;
    ss << "Any example text\n";
    ss << "is being added to the stream\n";

    // converting to string
    std::string text = ss.str();

    std::cout << text;

    return 0;
}