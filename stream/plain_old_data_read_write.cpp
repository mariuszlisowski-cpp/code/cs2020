// POD read write
// ~ Plain Old Data structure - no constructors, virual

#include <cstring>
#include <iostream>
#include <fstream>
#include <string>

// Plaind Old Data class
class Pod {
public:
    void DoSth() {}
    bool ReturnTrue() { return true; }
    bool ReturnFalse() { return false; }

    void SetName(std::string name) { strncpy(name_, name.data(), 15); }
    void SetIndex(size_t index) { index_ = index; }
    void SetAverage(double average) { average_ = average; }

    std::string GetName() const { return name_; }
    size_t GetIndex() const { return index_; }
    double GetAverage() const { return average_; }

private:
    char name_[15];
    size_t index_;
    double average_;
};

int main() {
    // Pod mateusz{"Mariusz", 123456, 5.0};
    Pod mariusz;
    mariusz.SetName("Mariusz");
    mariusz.SetIndex(123456);
    mariusz.SetAverage(5.0);

    // POD write
    std::fstream pod("pod_data", pod.binary | pod.out);
    if (pod.is_open()) {
        pod.write(reinterpret_cast<char*>(&mariusz), sizeof(Pod));
        pod.close();
    }

    // POD read
    pod.open("pod_data", pod.binary | pod.in);
    Pod mariusz_read;
    if (pod.is_open()) {
        pod.read(reinterpret_cast<char*>(&mariusz_read), sizeof(Pod));
        std::cout << "Name: " << mariusz_read.GetName() << '\n';
        std::cout << "Index: " << mariusz_read.GetIndex() << '\n';
        std::cout << "Average: " << mariusz_read.GetAverage() << '\n';
        pod.close();
    }
}
