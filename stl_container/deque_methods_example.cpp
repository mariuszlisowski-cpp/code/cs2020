// deque methods example

#include <deque>
#include <iostream>

template <class T>
void showList(const T& l) {
    for (const auto& e : l)
        std::cout << e << ' ';
    std::cout << '\n';
}

int main() {
    std::deque<int> dq;

    // using initializer list
    dq.insert(dq.begin(), { 1, 2, 3, 4, 5 });
    showList(dq);

    // erasing (random access iterator)
    dq.erase(dq.begin() + 1);
    dq.erase(dq.begin() + 2);
    showList(dq);

    // writing front & back
    dq.emplace_front(30);
    dq.emplace_back(30);
    showList(dq);

    // inserting (random access iterator)
    dq.emplace(dq.begin() + 3, 20);
    showList(dq);

    return 0;
}