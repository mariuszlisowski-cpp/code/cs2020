// 

#include <iostream>
#include <map>

int main() {
    
    std::map<int, int> mp;

    mp.insert(std::make_pair(1, 10));
    mp.insert(std::make_pair(2, 20));

    for (auto e : mp)
        std::cout << e.first << " :-> " << e.second << std::endl;
    
    return 0;
}