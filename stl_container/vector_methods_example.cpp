// vector methods example

#include <algorithm>  // remove
#include <iostream>
#include <vector>

using namespace std;

void showVec(vector<int> v) {
    for (const auto& e : v)
        cout << e << " ";
    cout << '\n';
}

void showVecReverse(vector<int> v) {
    for (auto it = v.rbegin(); it != v.rend(); ++it)
        cout << *it << " ";
    cout << '\n';
}

int main() {
    int arr[]{ 10, 20, 30 };
    vector<int> vec{ 1, 2, 3, 4, 5, 6 };

    // accessing first & last
    cout << vec.front() << endl;
    cout << vec.back() << endl;

    // erasing
    vec.erase(vec.begin());
    showVec(vec);

    // inserting
    vec.emplace_back(5);
    vec.emplace(vec.begin(), 12);
    vec.insert(vec.begin(), 2, 99);
    vec.insert(vec.begin(), arr, arr + 3);
    showVec(vec);
    
    // using reverse iterators
    showVecReverse(vec);

    // removing (semi)
    auto it = remove(vec.begin(), vec.end(), 5);  // move all occurences
    vec.erase(it, vec.end());                     // cut moved occurences
    // or..
    // vec.erase(remove(vec.begin(), vec.end(), 5), vec.end());
    showVec(vec);

    // shrinking
    cout << "Size: " << vec.size() << endl;
    cout << "Capacity: " << vec.capacity() << endl;
    vec.shrink_to_fit();
    cout << "Capacity: " << vec.capacity() << endl;

    // clearing
    vec.clear();
    vec.erase(vec.begin(), vec.end());
    cout << "Size: " << vec.size() << endl;
    cout << "Capacity: " << vec.capacity() << endl;

    return 0;
}