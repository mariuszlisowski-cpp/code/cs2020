// map methods example

#include <iostream>
#include <map>
#include <string>

void displayMap(const std::map<int, std::string>& mp) {
    for (const auto& e : mp)
        std::cout << e.first << " : " << e.second << '\n';
    std::cout << '\n';
}

int main() {
    std::map<int, std::string> mp{
        { 1, "one" },
        { 2, "two" }
    };
    mp.emplace(std::make_pair(3, "!"));
    mp.emplace(std::make_pair(4, "four"));
    mp.emplace(std::make_pair(5, "!"));
    mp[6] = "six";
    mp.insert({ 7, "seven" });
    // mp.at(8) = "six";  // out-of-range

    // replacing values of keys
    mp[3] = "three";
    mp.at(5) = "five";

    displayMap(mp);

    // finding key with 4
    std::cout << "Found: " << mp[4] << '\n';  // .second
    // or..
    auto it = mp.find(4);
    if (it != mp.end())
        std::cout << "Found: " << it->first << '\n';

    // erasing key with 7
    mp.erase(7);

    displayMap(mp);

    return 0;
}