// list methods example
// ~ doubly linked list

#include <algorithm>
#include <array>
#include <iostream>
#include <list>

template <class T>
void showList(const T& l) {
    for (const auto& e : l)
        std::cout << e << ' ';
    std::cout << '\n';
}

int main() {
    std::list<int> list{ 0, 1, 2, 3, 4, 5 };
    showList(list);

    // erasing
    list.erase(std::next(list.begin(), 2));
    // ..alternatively
    // auto it = list.begin();
    // std::advance(it, 2);
    // list.erase(it);
    showList(list);

    // accessing front & back
    list.emplace_front(10);
    list.emplace_back(10);
    showList(list);

    // inserting
    list.emplace(std::next(list.begin(), 3), 20);
    // ..alternatively
    // it = std::next(list.begin(), 3);
    // list.emplace(it, 20);
    showList(list);

    // removing duplicates
    list.sort();    // sort required first
    list.unique();  // consecutive elements only
    showList(list);

    // reversing order
    list.reverse();
    showList(list);

    // swapping
    if (!list.empty())
        list.swap(list);

    // clearing
    list.clear();

    return 0;
}