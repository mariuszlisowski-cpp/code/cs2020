// forward list methods example
// ~ singly linked list

#include <forward_list>
#include <iostream>

template <class T>
void showList(const T& l) {
    for (const auto& e : l)
        std::cout << e << ' ';
    std::cout << '\n';
}

int main() {
    std::forward_list<int> fl{ 0, 1, 2, 3, 4, 5 };

    // erasing
    fl.erase_after(std::next(fl.before_begin(), 2));
    showList(fl);

    // inserting at front
    fl.emplace_front(10);
    showList(fl);

    // inserting at back
    auto before_end = fl.before_begin();
    for (const auto& e : fl)
        ++before_end;
    fl.emplace_after(before_end, 10);
    showList(fl);

    // inserting
    fl.insert_after(std::next(fl.before_begin(), 3), 20);
    // ..alternatively
    // ​list.emplace_after(list.end(), 10);
    // std::advance(it, 1);
    // list.emplace_after(it, 20);
    showList(fl);

    // swapping
    if (!fl.empty())
        fl.swap(fl);  // no avail

    // removing duplicates
    fl.sort();    // must be used first
    fl.unique();  // consecutive elements only
    showList(fl);

    // reversing
    fl.reverse();
    showList(fl);

    // clearing
    fl.clear();

    return 0;
}