// vector size & capacity

#include <iostream>
#include <vector>

using namespace std;

int main() {
    vector<int> v;
    cout << "size : capacity" << endl;

    cout << v.size() << " : " << v.capacity() << endl;

    v.resize(10, 5);
    cout << v.size() << " : " << v.capacity() << endl;

    v.reserve(20);
    cout << v.size() << " : " << v.capacity() << endl;

    v.shrink_to_fit();
    cout << v.size() << " : " << v.capacity() << endl;

    return 0;
}