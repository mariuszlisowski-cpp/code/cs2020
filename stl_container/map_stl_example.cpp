// map stl example
// ~ sorted ASSOCIATIVE container (keys cannot be modified)
// ~ keys ASSOCIATED with the corresponding values
// multimap allows duplicated items
// #insert() #begin() #find() #erase()


#include <iostream>
#include <map>

using namespace std;

int main() {
    map<char, int> mp;

    // insert new keys with values
    mp.insert(pair<char, int>('a', 10));
    mp.insert(make_pair('z', 20));
    mp.insert({'i', 90});
    mp['c'] = 30;

    // replace existing key
    mp['c'] = 50; // value can be modified but not a key

    // find 
    map<char, int>::iterator it;	
    it = mp.find('z');
    if (it != mp.end())
        cout << it->first << " found" << endl;

    for (it = mp.begin(); it != mp.end(); ++it)
        cout << it->first << " :-> " << it->second << endl;	

    mp.erase(it);
    mp.erase('i');


    return 0;
}