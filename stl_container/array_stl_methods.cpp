// array stl methods

#include <array>
#include <iostream>

template <size_t SIZE>
void display(std::array<int, SIZE> ar) {
    for (const int& e : ar)
        std::cout << e << ' ';
    std::cout << '\n';
}

int main() {
    constexpr size_t size = 5;

    std::array<int, size> arr1;
    arr1.fill(1);
    arr1.at(3) = 9;
    display(arr1);

    std::array<int, size> arr2;
    arr2.fill(5);
    display(arr2);

    arr1.swap(arr2);
    display(arr2);

    // raw pointer
    int* ptr = arr2.data();
    std::cout << *ptr;

    return 0;
}
