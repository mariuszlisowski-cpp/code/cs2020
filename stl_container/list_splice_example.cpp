// list splice example

#include <iostream>
#include <list>
 
std::ostream& operator<<(std::ostream& ostr, const std::list<int>& list) {
    for (auto &i : list) {
        ostr << " " << i;
    }
    return ostr;
}
 
int main ()
{
    std::list<int> list1 = { 1, 2, 3, 4, 5 };
    std::list<int> list2 = { 10, 20, 30, 40, 50 };
    
    // display lists only
    std::cout << "list1: " << list1 << "\n";
    std::cout << "list2: " << list2 << "\n"; // now empty
    std::cout << "--------------------------------" << std::endl;
 
    // move one list to another
    list1.splice(list1.begin(), list2);
    std::cout << "list1: " << list1 << "\n";
    std::cout << "list2: " << list2 << "\n"; // now empty
    std::cout << "--------------------------------" << std::endl;
    
    // move one list to another (except the last element)
    list2.splice(list2.begin(), list1, list1.begin(), --list1.end());
    std::cout << "list1: " << list1 << "\n"; // now empty
    std::cout << "list2: " << list2 << "\n";
    std::cout << "--------------------------------" << std::endl;

    // move part of one list to another (at the begining)
    auto it = list2.begin();
    std::advance(it, 5);
    list1.splice(list1.begin(), list2, it, list2.end());
    std::cout << "list1: " << list1 << "\n";
    std::cout << "list2: " << list2 << "\n";
    std::cout << "--------------------------------" << std::endl;

    return 0;
}