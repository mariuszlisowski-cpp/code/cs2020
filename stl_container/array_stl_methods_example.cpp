// array methods example

#include <array>
#include <iostream>

template <class T>
void showArray(const T& arr) {
    for (const auto& e : arr)
        std::cout << e << ' ';
    std::cout << '\n';
}

template <class T>
void showArrayReverse(const T& arr) {
    for (auto it = arr.rbegin(); it != arr.rend(); ++it)
        std::cout << *it << ' ';
    std::cout << '\n';
}

int main() {
    std::array<int, 10> arr1;
    arr1.fill(5);

    // accessing front & back
    if (!arr1.empty())
        std::cout << arr1.front() << '\n';
    if (arr1.size())
        std::cout << arr1.back() << '\n';

    // modifying
    arr1[3] = 7;
    showArray(arr1);

    // filling
    std::array<int, 10> arr2;
    arr2.fill(0);
    showArray(arr2);

    // swapping
    arr1.swap(arr2);

    // using iterators
    auto beg = arr1.begin();
    auto end = arr1.end() - 1;
    std::cout << (*beg = 9) << '\n';
    std::cout << (*end = 1) << '\n';

    // using reverse iterators
    showArrayReverse(arr1);
    showArrayReverse(arr2);

    return 0;
}