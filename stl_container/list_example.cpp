// list example

#include <iostream>
#include <list>

void showList(std::list<int>);

int main() {

    std::list<int> ls {1, 8, 9, 3, 5}	;
    showList(ls);

    // iterator
    std::cout << "First: " << *ls.begin() << std::endl;
    std::cout << "Last:  " << *ls.end() << std::endl;

    // reference
    std::cout << "First: " << ls.front() << std::endl;
    std::cout << "Last:  " << ls.back() << std::endl;
    
    ls.front() = 2; // can be modified (reference)
    ls.back() = 6;
    showList(ls);
    
    // reference
    std::cout << "First: " << ls.front() << std::endl;
    std::cout << "Last:  " << ls.back() << std::endl;
    
    // move to index..
    int n = 2;
    std::list<int>::iterator it = ls.begin();
    for (int i = 0; i < n; ++i) {
        it++;
    }
    std::cout << "At index " << n << ": " << *it << std::endl;

    //..or
    auto is = ls.begin();
    auto nx = std::next(is, n);
    std::cout << "At index " << n << ": " << *nx << std::endl;

    //..or
    auto ir = ls.begin();
    std::advance(ir, n);
    std::cout << "At index " << n << ": " << *ir << std::endl;

    ls.push_front(7); // can add at front
    ls.push_back(1);
    showList(ls);

    ls.sort(); // build-in sorting
    showList(ls);

    return 0;
}

void showList(std::list<int> l) {
    for (auto e : l)
        std::cout << e << " ";
    std::cout << std::endl;
}