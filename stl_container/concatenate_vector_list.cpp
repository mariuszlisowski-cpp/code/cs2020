// concatenate vector & list

#include <iostream>
#include <list>
#include <string>
#include <vector>
#include <map>

std::map<int, std::string> createMap(const std::vector<int>& v, 	
                                     const std::list<std::string>& s) {
    std::map<int, std::string> m;

    // auto it = s.begin();
    std::list<std::string>::const_iterator it = s.begin();
    for (int i = 0; i < v.size(); ++i) {
        m[v[i]] = *it++;
        // or
        // m.insert({v[i], *it++});
    }
    return m;
}


int main() {
    std::vector<int> vec{1, 2, 3, 4, 5};
    std::list<std::string> list{"One", "Two", "Three", "Four", "Five"};
    
    auto map = createMap(vec, list);
    
    for (const auto& pair : map)
        std::cout << pair.first << " | " << pair.second << '\n';
    return 0;
}