// map example
// bst tree sorted by key

#include <iostream>
#include <map>
#include <string>

void Print(const std::map<size_t, std::string>& map) {
    for (const auto& pair : map) {
        std::cout << pair.first << " | " << pair.second << '\n';
    }
}
int main() {
    std::map<size_t, std::string> discs {
        {1, "The Lord of the Rings: The Fellowship of the Ring"},
        {2, "The Lord of the Rings: The Two Towers"},
        {3, "The Lord of the Rings: The Return of the King"}
    };
    Print(discs);
    
    std::cout << "\nAfter adding a new element\n";
    discs[4] = "Harry Potter";
    Print(discs);
    
    std::cout << "\nAfter modification of an element\n";
    discs[4] = "Harry Potter and the Philosopher's Stone";
    Print(discs);
}