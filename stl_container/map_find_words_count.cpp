// map example
// ~ finds all N-letters words

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

int main() {
    std::multimap<int, std::string> names;
    std::vector<std::pair<int, std::string>> result;

    // filling a map
    names.insert({5, "Ala"});
    names.insert({5, "Ma"});
    names.insert({5, "Kota"});
    names.insert({5, "A"});
    names.insert({5, "Kot"});
    names.insert({5, "Ma"});
    names.insert({5, "Ale"});

    // searching all N-letter words
    size_t LETTERS = 3;
    std::copy_if(names.begin(), names.end(),
                 std::back_inserter(result),
                 [&LETTERS](const auto& pair) {
                     return pair.second.size() == LETTERS;
                 });
    // outputting resoult
    std::for_each(result.begin(), result.end(),
                  [](const auto& pair) {
                      std::cout << pair.second << std::endl;
                  });

    return 0;
}
