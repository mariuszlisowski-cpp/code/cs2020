// concatenate vector

#include <iostream>
#include <vector>
#include <algorithm>

std::vector<int> concatenateVector(const std::vector<int>& v1, 
                                   const std::vector<int>& v2) {
    std::vector<int> result;
    size_t size = std::max(v1.size(), v2.size());
    for (size_t i = 0; i < size; ++i) {
        if (i < v1.size())
            result.push_back(v1[i]);
        if (i < v2.size())
        result.push_back(v2[i]);
    }
    return result;	
}

int main() {
    std::vector<int> vec1 {1, 2, 3, 4, 5};
    std::vector<int> vec2 {10, 20, 30, 40, 50, 60, 70, 80, 90};
    auto vec = concatenateVector(vec1, vec2);

    for (const auto& el : vec) {
        std::cout << el << " ";
    }

    return 0;
}