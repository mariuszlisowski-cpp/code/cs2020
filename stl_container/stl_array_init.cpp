// array stl initialization & typeid

#include <array>
#include <iostream>
#include <typeinfo>

// regular array initialization
int arr[][5]{ 
     { 0, 1, 2, 3, 4},
     { 0, 1, 2, 3, 4},
     { 0, 1, 2, 3, 4}
};

// stl array initialization (extra curly braces)
std::array<std::array<int, 5>, 3> stl_arr {
    { { 1, 2, 3, 4, 5 },
      { 1, 2, 3, 4, 5 },
      { 1, 2, 3, 4, 5 } }
};

// function returning stl array
std::array<std::array<int, 10>, 3> init_arr() {
    return {
        std::array<int, 10>
            { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 },
            { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 },
            { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }
    };
}

void displayRange(std::array<std::array<int, 10>, 3> result) {
    for (const auto& row : result) {
        for (const auto& col : row) {
            std::cout << col << ' ';
        }
        std::cout << '\n';
    }
}

void displayRegular(std::array<std::array<int, 10>, 3> result) {
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 10; ++j) {
            std::cout << result[i][j] << ' ';
        }
        std::cout << '\n';
    }
}

int main() {
    using MyStlArray = std::array<std::array<int, 10>, 3>;
    MyStlArray result = init_arr();
    // or..
    // auto result = init_arr();
    
    displayRegular(result);
    
    std::cout << *(typeid(result).name()) << '\n';
    
    displayRange(result);

    std::cout << typeid(result).name() << '\n';

    return 0;
}
        