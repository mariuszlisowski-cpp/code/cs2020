#include <iostream>
#include <stdexcept>

struct TalkingObjectOutside {
    TalkingObjectOutside() { std::cout << "Constructor outside" << '\n'; }
    ~TalkingObjectOutside() { std::cout << "Destructor outside" << '\n'; }
};
struct TalkingObjectInside {
    TalkingObjectInside() { std::cout << "Constructor inside" << '\n'; }
    ~TalkingObjectInside() { std::cout << "Destructor inside" << '\n'; }
};

void foo() {
    throw std::runtime_error("Foo error");
}

// try the whole function
void bar() try {
    TalkingObjectInside inside; // object destructed
    foo();
} catch (const std::exception&) { // exception caught here
    std::cout << "Exception caught" << '\n';
    throw; // throw again the same exception (here std::runtime_error)
}

int main() {
    TalkingObjectOutside outside;
    
    try {
        bar();
    } catch (std::runtime_error const& ex) {
        std::cout << "std::runtime_error: " << ex.what() << '\n';
    }

    return 0;
    // outside object unwinded here
}
