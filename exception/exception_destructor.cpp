// THROWING AN EXCEPTION DURING STACK UNWINDING

#include <iostream>
#include <stdexcept>

struct TalkingObject {
    TalkingObject() { std::cout << "Constructor" << '\n'; }
    ~TalkingObject() { std::cout << "Destructor" << '\n'; }
};

// DO NOT THROW EXCEPTION IN DESTRUCTOR
struct ThrowingObject {
    ThrowingObject() { std::cout << "Throwing c-tor\n"; }
    ~ThrowingObject() {
        throw std::runtime_error("error in destructor"); // std::terminate() called
    }
};

void foo() {
    throw std::runtime_error("Error");
}
int main() {
    TalkingObject outside;
    try {
        ThrowingObject inside; // throwing exception here from destructor
        foo();
    } catch (std::exception const&) {
        std::cout << "std::exception" << '\n';
        throw;
    }

    return 0;
}
