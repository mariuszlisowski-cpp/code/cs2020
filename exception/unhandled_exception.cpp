#include <iostream>
#include <stdexcept>

struct TalkingObject {
    TalkingObject() { std::cout << "Constructor" << '\n'; }
    ~TalkingObject() { std::cout << "Destructor" << '\n'; }
};

void foo() {
    throw std::runtime_error("Foo error");
}

void bar() {
    try {
        TalkingObject inside;
        foo();
    } catch (std::logic_error const&) { // runtime_error do not inherit from logic_error
        std::cout << "std::logic_error" << '\n';  // exception NOT handled here
    }
}

int main() {
    TalkingObject outside;

    // NOT handled exception
    bar();

    // handled exception (but NOT run here)
    try {
        bar();
    } catch (const std::runtime_error& e) {
        std::cerr << e.what() << '\n';
    }
}
