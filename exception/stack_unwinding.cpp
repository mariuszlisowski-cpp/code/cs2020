#include <iostream>
#include <stdexcept>

struct TalkingObjectOutside {
    TalkingObjectOutside() { std::cout << "Constructor outside" << '\n'; }
    ~TalkingObjectOutside() { std::cout << "Destructor outside" << '\n'; }
};
struct TalkingObjectInside {
    TalkingObjectInside() { std::cout << "Constructor inside" << '\n'; }
    ~TalkingObjectInside() { std::cout << "Destructor inside" << '\n'; }
};

void foo() {
    throw std::runtime_error("Error");
}

int main() {
    TalkingObjectOutside outside;
    
    try {
        TalkingObjectInside inside; // unwinded from stack
        foo();
    } catch (std::runtime_error const& ex) {
        std::cout << "std::runtime_error: " << ex.what() << '\n';
    } catch (std::exception const&) {
        std::cout << "std::exception" << '\n'; // not used here
    }

    return 0;
    // outside object unwinded here
}
