#include <iostream>
#include <stdexcept>

class FooError : public std::runtime_error {
public:
    FooError() : std::runtime_error("foo function is broken") {}
};

void foo() {
    throw std::runtime_error("Any error");
}

void bar() {
    throw FooError();
}

int main() {
    try {
        foo();
    } catch (std::runtime_error const& e) {
        std::cerr << "Runtime: " << e.what() << '\n';
    } catch (const std::exception& e) {
        std::cerr << "Exception: " << e.what() << '\n';
    } catch (...) {
        std::cerr << "Unknown exception" << '\n';
    }

    try {
        bar();
    } catch (FooError const& e) {
        std::cerr << "Foo: " << e.what() << '\n';
    }
    try {
        bar();
    } catch (std::runtime_error const& e) {
        std::cerr << "Runtime: " << e.what() << '\n';
    }
    try {
        bar();
    } catch (std::exception const& e) {
        std::cerr << "Exception: " << e.what() << '\n';
    }
    try {
        bar();
    } catch (...) {
        std::cerr << "Unknown exception" << '\n';
    }

    return 0;
}
