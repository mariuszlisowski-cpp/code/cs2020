// functor usage

#include <algorithm>
#include <iostream>
#include <vector>

// used as a functor because..
struct Print {
    // ..contains a function call operator
    void operator()(int el) {
        std::cout << el << ' ';
    }
};

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5 };

    // inside for_each
    std::for_each(std::begin(v), std::end(v), Print{});
    std::cout << '\n';

    Print print;
    for (const auto el : v) {
        print(el);
    }

    return 0;
}