// functor comparision

#include <algorithm>
#include <iostream>
#include <vector>

// BEST PRACTICE
class AddValue {
    int val;
public:
    // initializes private variable
    AddValue(int param):val(param) {}

    // arg captured from an algorithm
    void operator()(int arg) {
        std::cout << arg + val << ' ';
    }
};

// regular function as a functor
void add2(int arg) {
    std::cout << arg + 2 << ' ';
}

// accepts constexpr only
template<int val>
void addVal(int arg) {
    std::cout << val + arg << ' ';
}

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5 };

    // not scalable
    // algorithm sends an argument to a functor
    std::for_each(std::begin(v), std::end(v), add2);
    std::cout << '\n';

    // constexpr only
    constexpr int x = 3;
    std::for_each(std::begin(v), std::end(v), addVal<x>);
    std::cout << '\n';

    // best
    std::for_each(std::begin(v), std::end(v), AddValue(4));

    return 0;
}
