// functor buil-in binding

#include <algorithm>
#include <deque>
#include <iostream>
#include <set>

bool needCopy(int el) {
    return (el > 20) || (el < 5);
}

void displayDeq(std::deque<int>);

int main() {
    std::set<int> myset{ 3, 1, 25, 7, 12 };  // input
    std::deque<int> d1;                      // output
    std::deque<int> d2;                      // output
    std::deque<int> d3;                      // output

    // checking & outputting to queue when (x > 20) || (x < 5)
    std::transform(myset.begin(), myset.end(),  // source
                   std::back_inserter(d1),      // destination
                   std::bind(std::logical_or<bool>(),
                    std::bind(std::greater<int>(), std::placeholders::_1, 20),
                    std::bind(std::less<int>(), std::placeholders::_1, 5))
                   );
    displayDeq(d1);

    // same using a function
    std::transform(myset.begin(), myset.end(),  // source
                   std::back_inserter(d2),      // destination
                   needCopy);
    displayDeq(d2);

    // copying using lambda function
    std::copy_if(myset.begin(), myset.end(),  // source
                 std::back_inserter(d3),      // destination
                 [](int el) {
                    return (el > 20) || (el < 5);
                });
    displayDeq(d3);

    return 0;
}

// display
void displayDeq(std::deque<int> d) {
    for (const auto& el : d) {
        std::cout << el << ' ';
    }
    std::cout << '\n';
}
