// lambda unique pointer capture

#include <iostream>
#include <memory>

int main() {
    std::unique_ptr<int> unique = std::make_unique<int>(9);

    // no arguments accepted hence no ()
    auto labmda = [unique = std::move(unique)] {
        return *unique;
    };

    std::cout << labmda() << std::endl;

    return 0;
}