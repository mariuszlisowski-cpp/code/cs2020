// nested namespaces

#include <iostream>

// modern c++17
namespace A::B::C {

}

// old way
namespace A {
    namespace B {
        namespace C {


        } // namespace C
    } // namespace B
} // namespace A

int main() {

    return 0;
}