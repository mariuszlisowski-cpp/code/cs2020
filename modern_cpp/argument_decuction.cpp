// argument decuction
// -std=c++17

#include <iostream>

int main() {
    std::pair<int, double> pair0;
    pair0.first = 1; // explicit initialization
    pair0.second = 1.1; // explicit initialization

    std::pair<int, double> pair1(1, 2.2); // c'tor init
    std::pair pair2(2, 3.3); // c'tor init (argument decuction)

    auto pair3 = std::make_pair<int, double>(3, 4.4);
    auto pair4 = std::make_pair(3, 4.4); // argument decuction

    return 0;
}
