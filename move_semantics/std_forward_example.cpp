// std forward example

#include <iostream>

class Gadget {};

void f(const Gadget&)       { std::cout << "const Gadget&\n"; }
void f(Gadget&)             { std::cout << "Gadget&\n"; }
void f(Gadget&&)            { std::cout << "Gadget&&\n"; }

// void use(const Gadget& g)   { f(g); }               // calls f(const Gadget&)
// void use(Gadget& g)         { f(g); }               // calls f(Gadget&)
// void use(Gadget&& g)        { f(std::move(g)); }    // calls f(Gadget&&)

// instead of three above function overrides
template <typename Gadget>
void use(Gadget&& g) {
    // f(g);                   // type is treated as l-value unconditionally
    // f(std::move(g));        // type is treated as r-value unconditionally

    // pass type as r-value if r-value was passed,
    // pass as l-value otherwise
    f(std::forward<Gadget>(g)); // forwards original type to f()
}

int main() {
    const Gadget cg;
    Gadget g;
    
    use(cg);            // calls use(const Gadget&) then calls f(const Gadget&)
    use(g);             // calls use(Gadget&) then calls f(Gadget&)
    use(Gadget());      // calls use(Gadget&&) then calls f(Gadget&&)

    return 0;
}
