// universal reference
// ~ accepts l-value & r-value

#include <utility>

// must be a template
template <typename T>
int takeAll(T&& a) {  // universal reference
    return a;
}

int take(int&& a) {
    return a;
}

int main() {
    // templated function call
    int i = 10;
    takeAll(5);  // r-value
    takeAll(i);  // l-value

    // typed function call
    take(5);             // r-value
    take(std::move(i));  // r-value

    return 0;
}