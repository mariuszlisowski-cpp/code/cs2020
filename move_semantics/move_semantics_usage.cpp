// move semantics usage

#include <iostream>
#include <string>

template <typename T>
class Container {
public:
    void insert(const T& item) {};  // inserts a copy of item
    void insert(T&& item) {};       // moves item into container
};

int main() {
    Container<std::string> c;
    std::string str = "text";

    c.insert(str);             // lvalue -> insert(const std::string&)
                               // inserts a copy of str, str is used later
    c.insert(str + str);       // rvalue -> insert(string&&)
                               // moves temporary into container
    c.insert("text");          // rvalue -> insert(string&&)
                               // moves temporary into container
    c.insert(std::move(str));  // rvalue -> insert(string&&)
                               // moves str into container, str is no longer used
    
    // std::cout << str << std::endl; // undefined behaviour
}
