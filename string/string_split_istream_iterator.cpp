// string split istream iterator
// #stringstream #istream_iterator

// #include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

template <class Container>
void printContainer(Container con) {
    for (const auto& el : con) {
        std::cout << el << '\n';
    }
}

int main() {
    std::string str{ "Split string example word by word" };
    std::istringstream iss(str);

    std::vector<std::string> words(
        std::istream_iterator<std::string>{ iss },
        std::istream_iterator<std::string>());
    // or..
    std::vector<std::string> wordsA(
        (std::istream_iterator<std::string>(iss)),
        std::istream_iterator<std::string>());


    std::cout << str << '\n';
    printContainer(words);

    return 0;
}
