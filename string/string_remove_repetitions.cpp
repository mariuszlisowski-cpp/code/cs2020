// remove repetitions in a string

#include <algorithm>
#include <iostream>
#include <iostream>
#include <iterator>
#include <numeric>
#include <sstream>
#include <vector>

template <class Container>
void printContainer(Container con) {
    for (const auto& el : con) {
        std::cout << el << '\n';
    }
}

void removeRepetitions(std::string& str) {
    std::istringstream iss(str);

    std::vector<std::string> words(
        std::istream_iterator<std::string>{ iss },
        std::istream_iterator<std::string>());

    // trick to leave ending dot (in a sentence)
    std::reverse(words.begin(), words.end());

    // leaving letters only
    auto areLetters = [](auto letter) {
        return !isalnum(letter) && isprint(letter);
    };
    // comparing alfanums only
    auto compare = [areLetters](auto lhs, auto rhs) {
        lhs.erase(std::remove_if(lhs.begin(),
                                 lhs.end(),
                                 areLetters),
                  lhs.end());
        rhs.erase(std::remove_if(rhs.begin(),
                                 rhs.end(),
                                 areLetters),
                  rhs.end());

        return lhs == rhs;
    };
    // rejecting the second equal
    words.erase(std::unique(words.begin(),
                            words.end(),
                            compare),
                words.end()); // vector already reversed

    // verbose
    printContainer(words);

    // adding spaces & joining into a string
    str = std::accumulate(words.rbegin(),
                          words.rend(),
                          std::string(),
                          [](std::string s1, std::string s2) {
                              return s1.empty() ? s2 : s1 + " " + s2;
                          });
}

int main() {
    // std::string s{ "Ala ma ma kota, a kot ma ma Ale Ale." };
    // std::string s{ "Ala ma ma kota, kota a kot ma ma Ale Ale." };
    std::string s{ "Ala -ma +ma kota, a kot -ma +ma -Ale +Ale." };
    
    std::cout << s << '\n';
    removeRepetitions(s);
    std::cout << s << '\n';

    return 0;
}
